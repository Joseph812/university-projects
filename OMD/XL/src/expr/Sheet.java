package expr;

import java.util.HashMap;
import java.util.Map;
import java.util.Observable;
import java.util.Set;

public class Sheet extends Observable implements Environment {

  private CellFactory cellFactory = new CellFactory();
  private Map<String, Cell> map = new HashMap<String, Cell>();
  private String clickedAddress;
  private String errorMsg;

  public Sheet() {
    this.clickedAddress = "A1";
  }

  private void setAndNotify() {
    setChanged();
    notifyObservers();
  }

  public void set(String content) {
    Cell newCell;
    try {
      newCell = cellFactory.build(clickedAddress, content, map, this);
      map.put(clickedAddress, newCell);
      errorMsg = "";

    } catch (Exception e) {
      errorMsg = e.getMessage();

    }
    setAndNotify();

  }

  public boolean contains(String address) {
    return map.containsKey(address);
  }

  public double value(String address) {
    Cell cell = map.get(address);
    return (cell == null) ? 0.0 : cell.value(this);
  }

  public String valueAsString(String address) {
    Cell cell = map.get(address);
    if (cell == null) {
      return "";
    } else {
      if (cell instanceof CommentCell) {
        return cell.toString().substring(1);
      } else {
        return Double.toString(cell.value(this));
      }
    }
  }

  public String expressionAt(String address) {
    Cell cell = map.get(address);
    if (cell == null) {
      return "";
    }
    return cell.toString();
  }

  public String getClickedAddress() {
    return clickedAddress;
  }

  public void setClickedAddress(String address) {
    clickedAddress = address;
    setAndNotify();
  }

  public void removeCell() {
    map.remove(clickedAddress);
    setAndNotify();
  }

  public void clear() { // to clear whole sheet
    map.clear();
    setAndNotify();
  }

  public String getErrorMsg() {
    return errorMsg;
  }

  public Set<Map.Entry<String, Cell>> getEntrySet() {
    return map.entrySet();
  }
}

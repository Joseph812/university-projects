package expr;

public class CommentCell extends Cell {

  String comment;

  public CommentCell(String comment) {
    this.comment = comment;
  }

  @Override
  public Double value(Sheet sheet) {
    return 0.0;
  }

  @Override
  public String toString() {
    return comment;
  }
}

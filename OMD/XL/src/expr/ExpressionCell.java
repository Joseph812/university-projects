package expr;

public class ExpressionCell extends Cell {

  private Expr expression;

  public ExpressionCell(Expr expression) {
    this.expression = expression;
  }

  @Override
  public Double value(Sheet sheet) {
    return expression.value(sheet);
  }

  @Override
  public String toString() {
    return expression.toString();
  }
}

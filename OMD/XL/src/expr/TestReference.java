package expr;

import java.util.Map;
import java.util.regex.Pattern;

public class TestReference {
  static ExprParser parser = new ExprParser();

  private static void testCyclicReference(String address, String expression, Map<String, Cell> m) throws Exception {
    if (expression.contains(address)) {
      throw new Exception("Cyclic Reference");
    }
    String[] tokens = expression.split("\\+|-|\\*|/");

    for (String s : tokens) {
      if (m.get(s) != null) {
        testCyclicReference(address, m.get(s).toString(), m);
      }
    }
  }

  private static void testEmptyReference(String address, String expression, Map<String, Cell> m) throws Exception {

    String[] tokens = expression.split("\\+|-|\\*|/");

    for (String s : tokens) {
      if (m.get(s) != null) {
        testEmptyReference(address, m.get(s).toString(), m);
      } else if (Pattern.matches("[a-zA-Z]{1}[0-9]{1}", s)) {
        throw new Exception("Empty Reference");
      }
    }
  }

  private static void testDivisionByZero(String address, String expression, Map<String, Cell> m, Sheet sheet)
      throws Exception {
    if (expression.contains("/")) {
      String[] divTokens = expression.split("/");
      for (int i = 1; i < divTokens.length; i++) {
        if (parser.build(divTokens[i]).value(sheet) == 0) {
          throw new Exception("Dividing by zero");
        }
      }
    }

    String[] tokens = expression.split("\\+|-|\\*|/");
    for (String s : tokens) {
      if (m.get(s) != null) {
        testDivisionByZero(address, m.get(s).toString(), m, sheet);
      }
    }
  }

  public static void test(String address, String expression, Map<String, Cell> m, Sheet sheet) throws Exception {
    testCyclicReference(address, expression, m);
    testEmptyReference(address, expression, m);
    testDivisionByZero(address, expression, m, sheet);
  }
}

package expr;

public abstract class Cell {
  public abstract Double value(Sheet sheet);

  public abstract String toString();
}

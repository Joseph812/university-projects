package expr;

import java.util.Map;

class CellFactory {
  
  Cell build(String currentAddress, String string, Map<String, Cell> m, Sheet sheet) throws Exception {
    if (string.charAt(0) == '#') {
      return new CommentCell(string);
    } else {
      Expr e = new ExprParser().build(string);
      TestReference.test(currentAddress, e.toString(), m, sheet);
      return new ExpressionCell(new ExprParser().build(string));
    }
  }
}

package gui;

import expr.Sheet;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Observable;
import java.util.Observer;

public class SlotLabel extends ColoredLabel implements Observer {

  private String address;
  private Sheet sheet;

  SlotLabel(Sheet sheet, String address) {
    super("                    ", Color.WHITE, RIGHT);
    this.address = address;
    this.sheet = sheet;
    sheet.addObserver(this);

    addMouseListener(new MouseAdapter() {
      public void mouseClicked(MouseEvent e) {
        sheet.setClickedAddress(address);
      }
    });
  }

  @Override
  public void update(Observable o, Object arg) {
    if (sheet.contains(address)) {
      setText(sheet.valueAsString(address));
    } else {
      setText("");
    }

    if (sheet.getClickedAddress().equals(address)) {
      setBackground(Color.YELLOW);
    } else {
      setBackground(Color.WHITE);
    }
  }
}

package gui;

import expr.Sheet;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class SlotLabels extends GridPanel {
  private SlotLabel currentLabel;
  private Sheet sheet;

  public SlotLabels(int rows, int cols, Sheet sheet) {
    super(rows + 1, cols);
    List<SlotLabel> labelList = new ArrayList<SlotLabel>(rows * cols);
    for (char ch = 'A'; ch < 'A' + cols; ch++) {
      add(new ColoredLabel(Character.toString(ch), Color.LIGHT_GRAY, SwingConstants.CENTER));
    }
    for (int row = 1; row <= rows; row++) {
      for (char ch = 'A'; ch < 'A' + cols; ch++) {
        SlotLabel label = new SlotLabel(sheet, ch + Integer.toString(row));
        add(label);
        labelList.add(label);
      }
    }

    SlotLabel firstLabel = labelList.get(0);
    firstLabel.setBackground(Color.YELLOW);
  }
}

package gui.menu;

import expr.Sheet;
import expr.XLPrintStream;
import gui.StatusLabel;
import gui.XL;

import javax.swing.*;
import java.io.FileNotFoundException;

class SaveMenuItem extends OpenMenuItem {
  private Sheet sheet;

  public SaveMenuItem(XL xl, StatusLabel statusLabel, Sheet sheet) {
    super(xl, statusLabel, "Save");
    this.sheet = sheet;
  }

  protected void action(String path) throws FileNotFoundException {
    XLPrintStream xlPS = new XLPrintStream(path);
    xlPS.save(sheet.getEntrySet());
  }

  protected int openDialog(JFileChooser fileChooser) {
    return fileChooser.showSaveDialog(xl);
  }
}

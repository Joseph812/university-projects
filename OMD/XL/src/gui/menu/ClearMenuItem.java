package gui.menu;

import expr.Sheet;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

class ClearMenuItem extends JMenuItem implements ActionListener {
  private Sheet sheet;

  public ClearMenuItem(Sheet sheet) {
    super("Clear");
    this.sheet = sheet;
    addActionListener(this);
  }

  public void actionPerformed(ActionEvent e) {
    sheet.removeCell();
  }
}

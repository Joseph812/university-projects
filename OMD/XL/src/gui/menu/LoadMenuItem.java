package gui.menu;

import expr.Sheet;
import expr.XLBufferedReader;
import gui.StatusLabel;
import gui.XL;

import javax.swing.*;
import java.io.FileNotFoundException;

class LoadMenuItem extends OpenMenuItem {
  Sheet sheet;

  LoadMenuItem(XL xl, StatusLabel statusLabel, Sheet sheet) {
    super(xl, statusLabel, "Load");
    this.sheet = sheet;
  }

  protected void action(String path) throws FileNotFoundException {
    XLBufferedReader xlBR = new XLBufferedReader(path);
    xlBR.load(sheet);
  }

  protected int openDialog(JFileChooser fileChooser) {
    return fileChooser.showOpenDialog(xl);
  }
}

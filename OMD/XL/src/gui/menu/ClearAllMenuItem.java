package gui.menu;

import expr.Sheet;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

class ClearAllMenuItem extends JMenuItem implements ActionListener {
  Sheet sheet;

  public ClearAllMenuItem(Sheet sheet) {
    super("Clear all");
    this.sheet = sheet;
    addActionListener(this);
  }

  public void actionPerformed(ActionEvent e) {
    sheet.clear();
  }
}

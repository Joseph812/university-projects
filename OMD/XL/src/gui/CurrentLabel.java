package gui;

import expr.Sheet;

import java.awt.*;
import java.util.Observable;
import java.util.Observer;

public class CurrentLabel extends ColoredLabel implements Observer {

  private Sheet sheet;

  public CurrentLabel(Sheet sheet) {
    super("A1", Color.WHITE);
    this.sheet = sheet;
    sheet.addObserver(this);
  }

  @Override
  public void update(Observable o, Object arg) {
    setText(sheet.getClickedAddress());
  }
}

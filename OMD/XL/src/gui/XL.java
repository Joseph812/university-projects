package gui;

import expr.Sheet;
import gui.menu.XLMenuBar;

import javax.swing.*;
import java.awt.*;
import java.awt.print.PageFormat;
import java.awt.print.Printable;

import static java.awt.BorderLayout.*;

public class XL extends JFrame implements Printable {
  private static final int ROWS = 10, COLUMNS = 8;
  private XLCounter counter;
  private XLList xlList;
  private Sheet sheet;

  public XL(XL oldXL) {
    this(oldXL.xlList, oldXL.counter);
  }

  public XL(XLList xlList, XLCounter counter) {
    super("Untitled-" + counter);
    this.xlList = xlList;
    this.counter = counter;
    sheet = new Sheet();
    xlList.add(this);
    counter.increment();
    StatusLabel statusLabel = new StatusLabel(sheet);
    JPanel statusPanel = new StatusPanel(statusLabel, sheet);
    JPanel sheetPanel = new SheetPanel(ROWS, COLUMNS, sheet);
    Editor editor = new Editor(sheet);
    add(NORTH, statusPanel);
    add(CENTER, editor);
    add(SOUTH, sheetPanel);
    setJMenuBar(new XLMenuBar(this, xlList, statusLabel, sheet));
    pack();
    setDefaultCloseOperation(EXIT_ON_CLOSE);
    setResizable(false);
    setVisible(true);
  }

  public static void main(String[] args) {
    new XL(new XLList(), new XLCounter());
  }

  public int print(Graphics g, PageFormat pageFormat, int page) {
    if (page > 0)
      return NO_SUCH_PAGE;
    Graphics2D g2d = (Graphics2D) g;
    g2d.translate(pageFormat.getImageableX(), pageFormat.getImageableY());
    printAll(g2d);
    return PAGE_EXISTS;
  }

  public void rename(String title) {
    setTitle(title);
    xlList.setChanged();
  }
}

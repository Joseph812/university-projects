package gui;

import java.awt.*;
import java.util.Observable;
import java.util.Observer;

import expr.Sheet;

public class StatusLabel extends ColoredLabel implements Observer {

  Sheet sheet;

  public StatusLabel(Sheet sheet) {
    super("   ", Color.WHITE);
    this.sheet = sheet;
    sheet.addObserver(this);
  }

  public void update(Observable observable, Object object) {
    setText(sheet.getErrorMsg());
  }
}

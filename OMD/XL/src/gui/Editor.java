package gui;

import expr.Sheet;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Observable;
import java.util.Observer;

public class Editor extends JTextField implements Observer {

  private String currentAddress;
  private Sheet sheet;

  Editor(Sheet sheet) {
    this.sheet = sheet;
    sheet.addObserver(this);
    setBackground(Color.WHITE);
    addKeyListener(new KeyAdapter() {
      @Override
      public void keyPressed(KeyEvent e) {
        if (e.getKeyChar() == KeyEvent.VK_ENTER) {
          sheet.set(getText());
        }
      }
    });
  }

  @Override
  public void update(Observable o, Object arg) {
    currentAddress = sheet.getClickedAddress();
    setText(sheet.expressionAt(currentAddress));
  }
}

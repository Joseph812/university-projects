package operations;

import computer.*;
import word.Word;

public abstract class BinaryOperator implements Instruction {

  private Operand op1, op2;
  private Address address;

  BinaryOperator(Operand op1, Operand op2, Address address) {
    this.address = address;
    this.op1 = op1;
    this.op2 = op2;
  }

  protected abstract void op(Word w1, Word w2, Word w3);

  protected abstract String type();

  public void execute(Memory memory, ProgramCounter counter) {
    op(op1.getWord(memory), op2.getWord(memory), address.getWord(memory));
    counter.next();
  }

  @Override
  public String toString() {
    return type() + " " + op1.toString() + " and " + op2.toString() + " into " + address.toString();
  }
}

package operations;

import computer.Address;
import computer.Operand;
import word.Word;

public class Add extends BinaryOperator {

  public Add(Operand w1, Operand w2, Address address) {
    super(w1, w2, address);
  }

  public void op(Word w1, Word w2, Word destination) {
    destination.add(w1, w2);
  }

  public String type() {
    return "Add";
  }
}

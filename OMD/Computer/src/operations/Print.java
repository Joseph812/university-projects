package operations;

import computer.Address;
import computer.Instruction;
import computer.Memory;
import computer.ProgramCounter;

public class Print implements Instruction {

  private Address address;

  public Print(Address address) {
    this.address = address;
  }

  public void execute(Memory memory, ProgramCounter counter) {
    System.out.println(address.getWord(memory));
    counter.next();
  }

  @Override
  public String toString() {
    return "Print " + address.toString();
  }
}

package operations;

import computer.Instruction;
import computer.Memory;
import computer.Operand;
import computer.ProgramCounter;

public class JumpEq implements Instruction {

  private Operand w1, w2;
  private int index;

  public JumpEq(int index, Operand w1, Operand w2) {
    this.w1 = w1;
    this.w2 = w2;
    this.index = index;
  }

  public void execute(Memory memory, ProgramCounter counter) {
    if (w1.getWord(memory).equals(w2.getWord(memory))) {
      counter.jumpTo(index);
    } else counter.next();
  }

  @Override
  public String toString() {
    return "Jump to " + index + " if " + w1.toString() + " == " + w2.toString();
  }
}

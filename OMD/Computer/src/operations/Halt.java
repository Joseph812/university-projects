package operations;

import computer.Instruction;
import computer.Memory;
import computer.ProgramCounter;

public class Halt implements Instruction {

  public Halt() {}

  public void execute(Memory memory, ProgramCounter counter) {
    counter.jumpTo(-1);
  }

  @Override
  public String toString() {
    return "Halt";
  }
}

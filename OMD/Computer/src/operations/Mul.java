package operations;

import computer.Address;
import computer.Operand;
import word.Word;

public class Mul extends BinaryOperator {

  public Mul(Operand w1, Operand w2, Address address) {
    super(w1, w2, address);
  }

  public void op(Word w1, Word w2, Word destination) {
    destination.multiply(w1, w2);
  }

  public String type() {
    return "Multiply";
  }
}

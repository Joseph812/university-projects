package operations;

import computer.Address;
import computer.Instruction;
import computer.Memory;
import computer.ProgramCounter;
import word.Word;

public class Copy implements Instruction {

  private Word word;
  private Address address;

  public Copy(Word word, Address address) {
    this.word = word;
    this.address = address;
  }

  public void execute(Memory memory, ProgramCounter counter) {
    address.getWord(memory).copy(word);
    counter.next();
  }

  @Override
  public String toString() {
    return "Copy " + word.toString() + " to " + "[" + address.index() + "]";
  }
}

package operations;

import computer.Instruction;
import computer.Memory;
import computer.ProgramCounter;

public class Jump implements Instruction {

  private int index;

  public Jump(int index) {
    this.index = index;
  }

  public void execute(Memory memory, ProgramCounter counter) {
    counter.jumpTo(index);
  }

  @Override
  public String toString() {
    return "Jump to " + index;
  }
}

package word;

import computer.Memory;
import computer.Operand;

public abstract class Word implements Operand {

  public abstract void multiply(Word word1, Word word2);

  public abstract void add(Word word1, Word word2);

  public abstract void copy(Word word);

  public abstract Boolean equals(Word other);

  public Word getWord(Memory memory) {
    return this;
  }
}

package word;

public class LongWord extends Word {

  private long value;

  LongWord(long b) {
    value = b;
  }

  public String toString() {
    return String.valueOf(value);
  }

  public void multiply(Word word1, Word word2) {
    value = ((LongWord) word1).value * ((LongWord) word2).value;
  }

  public void add(Word word1, Word word2) {
    value = ((LongWord) word1).value + ((LongWord) word2).value;
  }

  public void copy(Word word) {
    value = ((LongWord) word).value;
  }

  @Override
  public Boolean equals(Word other) {
    return this.value == ((LongWord) other).value;
  }
}

package word;

public class ByteWord extends Word {

  private byte value;

  ByteWord(byte b) {
    value = b;
  }

  public String toString() {
    return String.valueOf(value);
  }

  public void multiply(Word word1, Word word2) {
    value = (byte) (((ByteWord) word1).value * ((ByteWord) word2).value);
  }

  public void add(Word word1, Word word2) {
    value = (byte) (((ByteWord) word1).value + ((ByteWord) word2).value);
  }

  public void copy(Word word) {
    value = ((ByteWord) word).value;
  }

  public Boolean equals(Word other) {

    return this.value == ((ByteWord) other).value;
  }
}

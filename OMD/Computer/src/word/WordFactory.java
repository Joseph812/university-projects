package word;

public interface WordFactory {

  public Word word(String value);
}

package word;

public class ByteWordFactory implements WordFactory {

  public ByteWord word(String value) {
    return new ByteWord(Byte.parseByte(value));
  }
}

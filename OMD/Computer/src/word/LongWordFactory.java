package word;

public class LongWordFactory implements WordFactory {

  public LongWord word(String value) {
    return new LongWord(Long.parseLong(value));
  }
}

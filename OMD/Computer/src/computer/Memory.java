package computer;

import word.Word;
import word.WordFactory;

public class Memory {

  private Word[] words;

  public Memory(int size, WordFactory factory) {
    this.words = new Word[size];

    for (int i = 0; i < words.length; i++) {
      words[i] = (factory.word("0"));
    }
  }

  public Word getWord(int index) {
    return words[index];
  }
}

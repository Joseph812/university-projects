package computer;

import word.Word;

public interface Operand {

  public Word getWord(Memory memory);
}

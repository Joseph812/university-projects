package computer;

import java.util.ArrayList;
import java.util.List;

public abstract class Program {

  private List<Instruction> instructions;

  Program() {
    instructions = new ArrayList<>();
  }

  void add(Instruction instruction) {
    instructions.add(instruction);
  }

  Instruction get(int n) {
    return instructions.get(n);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    int index = 0;
    for (Instruction instr : instructions) {
      sb.append(index).append(": ").append(instr.toString());
      sb.append("\n");
      index++;
    }

    return sb.toString();
  }
}

package computer;

public class Computer {

  private Memory memory;
  private Program program;

  Computer(Memory memory) {
    this.memory = memory;
  }

  void load(Program pgm) {
    this.program = pgm;
  }

  void run() {

    ProgramCounter counter = new ProgramCounter();

    while (counter.current() >= 0) {

      program.get(counter.current()).execute(memory, counter);
    }
  }
}

package computer;

import word.Word;

public final class Address implements Operand {

  private int index;

  Address(int index) {
    this.index = index;
  }

  public int index() {
    return index;
  }

  @Override
  public Word getWord(Memory memory) {
    return memory.getWord(index);
  }

  public String toString() {
    return "[" + index + "]";
  }
}

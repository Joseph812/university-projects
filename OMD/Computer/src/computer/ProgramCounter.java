package computer;

public class ProgramCounter {

  private int pos;

  ProgramCounter() {
    this.pos = 0;
  }

  public void next() {
    pos++;
  }

  public void jumpTo(int index) {
    pos = index;
  }

  int current() {
    return pos;
  }

  public Boolean hasStopped() {
    return pos == -1;
  }
}

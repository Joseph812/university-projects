package parse;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import datastructure.*;

public class ParsePersonalDetails {
	EnduroMap em;
	
	public ParsePersonalDetails() {
		em = new EnduroHashMap();
	}
	
	public ParsePersonalDetails(EnduroMap em) {
		this.em = em;
	}
	
	public void parseNameFile(File nameFile) throws FileNotFoundException {
		Scanner scan = new Scanner(nameFile);
		List<String> identifiers;
		String[] line;
		int nbr;
		identifiers = Arrays.asList(scan.nextLine().split("; ")); //gets column names
		
		while (scan.hasNextLine()) {
			line = scan.nextLine().split("; ");
			nbr = Integer.parseInt(line[0]);
			
			if (!em.containsNbr(nbr)) {
				em.addParticipant(new GeneralPerson(nbr));
			}
			
			for (int i = 0; i < identifiers.size(); i++) {	
				em.getPerson(nbr).setDetail(identifiers.get(i), line[i]);
			} 
		}
		
		em.setIdentifiers(identifiers);
		scan.close();
	}
	
}

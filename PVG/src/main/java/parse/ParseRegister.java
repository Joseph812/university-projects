package parse;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import datastructure.*;

public class ParseRegister {
	private EnduroMap em;
	
	public ParseRegister(EnduroMap em) {
		this.em = em;
	}
	
	public ParseRegister() {
		this.em = new EnduroHashMap();
	}
	
	public EnduroMap getEnduroMap() {
		return em;
	}

	public void parseStartFile(File file) throws FileNotFoundException {
		Scanner scan = new Scanner(file);
		String[] line;
		while (scan.hasNextLine()) {
			line = scan.nextLine().split("; ");
			if(line.length < 2){
				em.putStartTime(Integer.parseInt(line[0]), null);
			}
			em.putStartTime(Integer.parseInt(line[0]), EnduroLocalTime.parse(EnduroLocalTime.parseTime(line[1], "\\.", ":")));
		}
		scan.close();
		
	}
	public void parseEndFile(File file) throws FileNotFoundException {
		Scanner scan = new Scanner(file);
		String[] line;
		while (scan.hasNextLine()) {
			line = scan.nextLine().split("; ");
			em.putEndTime(Integer.parseInt(line[0]), EnduroLocalTime.parse(EnduroLocalTime.parseTime(line[1], "\\.", ":")));
		}
		scan.close();
	}
}

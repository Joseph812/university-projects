package datastructure;

public interface EnduroTime {

    @Override
    public boolean equals(Object obj);

    @Override
    public String toString();

    public String getTimeOrDefault(String defaultString);

    public boolean isReasonableTotalTime(String minimitid);

}
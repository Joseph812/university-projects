package datastructure;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class GeneralPerson implements Comparable<GeneralPerson>, Person{
	int participantNbr;
	Map<String, String> details;
	List<EnduroLocalTime> endTimes;
	List<EnduroLocalTime> startTimes;

	public GeneralPerson(Integer participantNbr) {
		this.participantNbr = participantNbr;
		details = new LinkedHashMap<String, String>();
		details.put("StartNr", participantNbr.toString());
		startTimes = new ArrayList<EnduroLocalTime>(1);
		endTimes = new ArrayList<EnduroLocalTime>();
	}

	public void putStartTime(EnduroLocalTime time) {
		startTimes.add(time);
	}

	public void putEndTime(EnduroLocalTime time) {
		endTimes.add(time);
		endTimes.sort((a,b) -> EnduroLocalTime.compare(a, b));
	}

	public List<EnduroLocalTime> getStartTimes() {
		if (startTimes.isEmpty()) {
			ArrayList<EnduroLocalTime> list = new ArrayList<EnduroLocalTime>();
			list.add(new EnduroLocalTime());
			return list;
		} else {
			return startTimes;
		}
	}

	public List<EnduroLocalTime> getEndTimes() {
		if (endTimes.isEmpty()) {
			ArrayList<EnduroLocalTime> list = new ArrayList<EnduroLocalTime>();
			list.add(new EnduroLocalTime());
			return list;
		} else {
			return endTimes;
		}
	}


	public Map<String, String> details() {
		return details;
	}

	public String details(String... args) {
		StringBuilder string = new StringBuilder();
		for (String arg: args) {
			string.append(details.get(arg) + "; ");
		}
		return string.toString();
	}

	public int getNbr() {
		return participantNbr;
	}

	public void setDetail(String identifier, String detail) {
		details.put(identifier, detail);
	}

	public int getNumberOfLaps(){

		return endTimes.size();
	}

	public String getEndTimeOrDefault(String defaultString, int index) {
		try {
			return endTimes.get(index).toString();
		} catch(NullPointerException npe) {
			return defaultString;
		} catch(IndexOutOfBoundsException ioobe) {
			return defaultString;
		}
	}

	public String getEndTimeOrDefault(String defaultString) {
		try {
			return endTimes.get(0).toString();
		} catch(NullPointerException npe) {
			return defaultString;
		} catch(IndexOutOfBoundsException ioobe) {
			return defaultString;
		}
	}

	public String getStartTimeOrDefault(String defaultString, int index) {
		try {
			return startTimes.get(index).toString();
		} catch(NullPointerException npe) {
			return defaultString;
		} catch(IndexOutOfBoundsException ioobe) {
			return defaultString;
		}
	}

	public String getStartTimeOrDefault(String defaultString) {
		try {
			return startTimes.get(0).toString();
		} catch(NullPointerException npe) {
			return defaultString;
		} catch(IndexOutOfBoundsException ioobe) {
			return defaultString;
		}
	}

	@Override
	public int compareTo(GeneralPerson other) {
		Integer otherEndSize = other.getEndTimes().size();
		if (otherEndSize == this.endTimes.size()) {
			return EnduroLocalTime.compare(
				EnduroLocalTime.between(
					other.getStartTimes().get(0),
					other.getEndTimes().get(otherEndSize - 1)),
				EnduroLocalTime.between(
					this.startTimes.get(0), 
					this.endTimes.get(endTimes.size() - 1)
				)
			);
		} else {
			return -otherEndSize.compareTo(this.endTimes.size()); //a computer is a rock we tricked into thinking
		}
	}

	public EnduroLocalTime getStartTime() {
		return startTimes.get(0);
	}

	public EnduroLocalTime getEndTime(int i) {
		return endTimes.get(i);
	}
}
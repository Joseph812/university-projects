package result;

import java.util.List;

import datastructure.EnduroMap;
import datastructure.GeneralPerson;

public class SortedLaps extends ResultLaps {
	private int plac = 1;
	
    public SortedLaps(EnduroMap em, String extraInfo) {
		super(em, extraInfo);
    }
	
	@Override
	protected List<GeneralPerson> getPersons() {
		return em.getSortedPersons();
	}

	@Override
	protected void addHeader() {
		rp.addHeader("Plac","StartNr","Namn", "#Varv", "Totaltid", "Varv1", "Varv2", "Varv3", "Start", "Varvning1", "Varvning2",
		"Mål");
	}

	@Override
	protected void nextRowStrategy(GeneralPerson p) {
		rp.addRow(Integer.toString(plac));
		plac++;
		rp.addCell(String.valueOf(p.getNbr()));
	}
}
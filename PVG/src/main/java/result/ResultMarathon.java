package result;

import datastructure.EnduroMap;
import datastructure.EnduroLocalTime;
import datastructure.GeneralPerson;

public abstract class ResultMarathon extends Result {

    public ResultMarathon(EnduroMap em, String extraInfo) {
        super(em, extraInfo);
    }

    @Override
    protected void totalTimeStrategy(GeneralPerson p) {
        rp.addCell(EnduroLocalTime.between(p.getStartTime(), p.getEndTime(p.getNumberOfLaps() - 1)).getTimeOrDefault("--.--.--"));
    }

    @Override
    protected void timeStrategy(GeneralPerson p) {
        rp.addCell(p.getEndTimeOrDefault(""));
    }

    @Override
    protected int maxEndTimes() {
        return 3;
    }
}
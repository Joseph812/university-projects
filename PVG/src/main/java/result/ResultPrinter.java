package result;

public class ResultPrinter extends EnduroPrinter {
    private int columnIndex;
    private int headerLength = 0;

    public ResultPrinter(){
        this.sb = new StringBuilder();
    }

    @Override
    public void addCell(String cell){
        super.addCell(cell);
        if (columnIndex + 1 > headerLength) {
            throw new IndexOutOfBoundsException("No header for cell.");
        }
    }

    @Override
    public void addRow(String firstCell){
        super.addRow(firstCell);
        columnIndex = 0;
    }

    public void addHeader(String ... header){
        for(String cell : header){
            if(headerLength == 0){
                sb.append(cell); 
            }else{
                super.addCell(cell);
            }
            headerLength++;
        }
    }

    public void addExtraInfo(String extraInfo) {
        if (extraInfo != "") {
            sb.append(extraInfo + "\n");
        }
    }

    public void addCredits() {
        sb.append("\n\nEnduro Timekeeper (patent pending), brought to you by Team Ten(tm), Copyright (2020)\n" + 
        "Enduro, Enduro Timekeeper and Timekeeper trademarks are properties of Team Ten(tm)");
    }

    public void addErrorCell(String errorCell){
        super.addCell(errorCell);
    }

    public void printFaultyNumbers() {
        
    }
}
package result;

import java.util.List;

import datastructure.EnduroMap;
import datastructure.GeneralPerson;

public class UnsortedMarathon extends ResultMarathon {
	
    public UnsortedMarathon(EnduroMap em, String extraInfo) {
		super(em, extraInfo);
    }
	
	@Override
	protected List<GeneralPerson> getPersons() {
		return em.getPersons();
	}

	@Override
	protected void addHeader() {
		rp.addHeader("StartNr","Namn", "Totaltid", "Start", "Mål");
	}

	@Override
	protected void nextRowStrategy(GeneralPerson p) {
		rp.addRow(String.valueOf(p.getNbr()));
	}
}
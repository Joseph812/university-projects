package result;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

abstract class EnduroPrinter {
    protected StringBuilder sb;

    public void generateFile(String fileName) throws IOException{
        File file = new File(fileName);
        file.delete();
        FileWriter fw = new FileWriter(file);
        fw.write(sb.toString());
		fw.close();
    }

    public void addCell(String cell){
        sb.append("; " + cell);
    }

    public void addRow(String firstCell){
        sb.append("\n" + firstCell);
    }

}
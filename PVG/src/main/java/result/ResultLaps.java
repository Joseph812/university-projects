package result;

import datastructure.EnduroMap;
import datastructure.EnduroLocalTime;
import datastructure.GeneralPerson;

public abstract class ResultLaps extends Result {

    public ResultLaps(EnduroMap em, String extraInfo) {
        super(em, extraInfo);
    }

    @Override
    protected void totalTimeStrategy(GeneralPerson p) {
        rp.addCell(Integer.toString(p.getNumberOfLaps()));
        rp.addCell(EnduroLocalTime.between(p.getStartTime(), p.getEndTime(p.getNumberOfLaps() - 1)).getTimeOrDefault("--.--.--"));

        for (int j = 0; j < 3; j++) {
            try {
                if (j == 0) {
                    rp.addCell(EnduroLocalTime.betweenOrDefault(p.getStartTime(), p.getEndTime(j), ""));
                } else {
                    rp.addCell(EnduroLocalTime.betweenOrDefault(p.getEndTime(j - 1), p.getEndTime(j), ""));
                }
            } catch (IndexOutOfBoundsException ioobe) {
                rp.addCell("");
            }
        }
    }

    @Override
    protected void timeStrategy(GeneralPerson p) {
        for (int j = 0; j < 3 - 1; j++) {
            if (p.getNumberOfLaps() - 1 == j) {
                rp.addCell("");
            } else {
                rp.addCell(p.getEndTimeOrDefault("", j));
            }
        }
    }

    @Override
    protected int maxEndTimes() {
        return 3;
    }
}
package result;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import datastructure.EnduroMap;
import datastructure.EnduroLocalTime;
import datastructure.GeneralPerson;

public abstract class Result {
    protected String extraInfo;
	protected EnduroMap em;
    protected List<String> orderedDetails;
	protected String minimalTime;
	protected ResultPrinter rp;
    
    public Result(EnduroMap em, String extraInfo) {
        this.em = em;
        this.rp = new ResultPrinter();
        this.orderedDetails = em.getIdentifiers();
        this.extraInfo = extraInfo;
        this.minimalTime = "00:15:00";
    }
    
    public void generateResult(String fileName) throws IOException {
        rp.addExtraInfo(extraInfo);
        addHeader();
        String startTime, endTime;
        int numLap;
        for (GeneralPerson p : getPersons()) {
            Map<String, String> details = p.details();
            numLap = p.getNumberOfLaps();
            
            startTime = p.getStartTimeOrDefault("Start?");
            endTime = p.getEndTimeOrDefault("Slut?", numLap - 1);
            
            nextRowStrategy(p);
            
            orderedDetails.forEach(s -> {
                if (!s.equals("StartNr")) {
                    if (details.get(s) != null) {
                        rp.addCell(details.get(s));
                    } else {
                        rp.addCell(s + " saknas?");
                    }
                }
            });
            
            totalTimeStrategy(p);
            
            rp.addCell(startTime);
            
            timeStrategy(p);
            
            rp.addCell(endTime);
            errorHandling(p.getNbr());
        }
        rp.addCredits();
        rp.generateFile(fileName);
    }
    
    protected void errorHandling(int participantNbr) {
        if (em.getStartTimes(participantNbr).size() > 1) {
            rp.addErrorCell("Flera starttider?" + redundantTimes(em.getStartTimes(participantNbr), 1));
        }
        if (em.getEndTimes(participantNbr).size() > maxEndTimes()) {
            rp.addErrorCell("Flera måltider?" + redundantTimes(em.getEndTimes(participantNbr), maxEndTimes()));
        }
		int numberOfStartTimes = em.getStartTimes(participantNbr).size();
        for (int i = 0 ; i < numberOfStartTimes ; i++){
			EnduroLocalTime temp = EnduroLocalTime.between(em.getStartTimes(participantNbr).get(i),
			em.getEndTimes(participantNbr).get(i));
            if (!temp.isReasonableTotalTime(minimalTime)) {
                rp.addErrorCell("Omöjlig Totaltid?");
            }
		}
    }
    
	protected String redundantTimes(List<EnduroLocalTime> list, int maxTimes) {
        StringBuilder sb = new StringBuilder();
		for (int i = maxTimes; i < list.size(); i++) {
            sb.append(" ");
			sb.append(list.get(i).toString());
		}
		return sb.toString();
	}

	protected int maxEndTimes() {
        return 1;
    }

    public void setMinimalTime(String minimalTime) {
        this.minimalTime = minimalTime;
    }

    protected abstract List<GeneralPerson> getPersons();
    
    protected abstract void addHeader();
    
    protected abstract void nextRowStrategy(GeneralPerson p);
    
    protected abstract void totalTimeStrategy(GeneralPerson p);

    protected abstract void timeStrategy(GeneralPerson p);
}
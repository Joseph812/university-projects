package result;

import java.util.List;

import datastructure.EnduroMap;
import datastructure.GeneralPerson;

public class UnsortedLaps extends ResultLaps {
	
    public UnsortedLaps(EnduroMap em, String extraInfo) {
		super(em, extraInfo);
    }
	
	@Override
	protected List<GeneralPerson> getPersons() {
		return em.getPersons();
	}

	@Override
	protected void addHeader() {
		rp.addHeader("StartNr","Namn", "#Varv", "Totaltid", "Varv1", "Varv2", "Varv3", "Start", "Varvning1", "Varvning2",
		"Mål");
	}

	@Override
	protected void nextRowStrategy(GeneralPerson p) {
		rp.addRow(String.valueOf(p.getNbr()));
	}
}
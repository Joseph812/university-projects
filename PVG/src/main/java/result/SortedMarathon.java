package result;

import java.util.List;

import datastructure.EnduroMap;
import datastructure.GeneralPerson;

public class SortedMarathon extends ResultMarathon {
	private int plac = 1;
	
    public SortedMarathon(EnduroMap em, String extraInfo) {
		super(em, extraInfo);
    }
	
	@Override
	protected List<GeneralPerson> getPersons() {
		return em.getSortedPersons();
	}

	@Override
	protected void addHeader() {
		rp.addHeader("StartNr","Namn", "Totaltid","Start", "Mål");
	}

	@Override
	protected void nextRowStrategy(GeneralPerson p) {
		rp.addRow(Integer.toString(plac));
		plac++;
		rp.addCell(String.valueOf(p.getNbr()));
	}
}
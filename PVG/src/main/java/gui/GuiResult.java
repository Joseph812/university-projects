package gui;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import result.*;
import datastructure.*;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import parse.ParsePersonalDetails;
import parse.ParseRegister;

public class GuiResult extends Application {
	FileChooser filechooser = new FileChooser();
	EnduroMap em = new EnduroHashMap();
	ParsePersonalDetails piP = new ParsePersonalDetails(em);
	ParseRegister piR = new ParseRegister(em);
	Result res;
	File startFile;
	File regFile1;
	File personFile;
	File regFile2;
	File regFile3;
	String unsortedLapResultFile;
	String sortedLapsResultFile;
	Boolean anyFile = false;
	ButtonExecute be = new ButtonExecute();
	String minimitime;
	Boolean marathonSelected = true;
	String competitionInfo;

	public static void main(final String[] args) {
		launch(args);
	}

	private synchronized void work() throws InterruptedException {
		while (true) {
			try {
				parseFiles();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			if(unsortedLapResultFile != null){
				if(marathonSelected) {
					res = new UnsortedMarathon(em, competitionInfo);
				} else {
					res = new UnsortedLaps(em, competitionInfo);
				}
				generateResult(unsortedLapResultFile);
				unsortedLapResultFile = null;
			}
			else if(sortedLapsResultFile != null) {
				if(marathonSelected) {
					res = new SortedMarathon(em,competitionInfo);
				} else {
					res = new SortedLaps(em, competitionInfo);
				}
				generateResult(sortedLapsResultFile);
				sortedLapsResultFile = null; 
			}
	
			wait();
		}
	}

	private void generateResult(String fileName){
		try {
			if (minimitime != null) {
			res.setMinimalTime(minimitime);
			}
			res.generateResult(fileName);
		} catch (final FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (final IOException e1) {
			e1.printStackTrace();
		}
	}

	private synchronized void ready() {
		notifyAll();
	}

	private void parseFiles() throws FileNotFoundException {
		if(personFile != null) piP.parseNameFile(personFile);
		if(regFile2 != null) piR.parseEndFile(regFile2);
		if(regFile3 != null) piR.parseEndFile(regFile3);
		if(regFile1 != null) piR.parseEndFile(regFile1);
		if(startFile != null) piR.parseStartFile(startFile);
	}



	@Override
	public void start(final Stage PrimaryStage) throws Exception {

		VBox headerBox = new VBox();
		VBox infoBox = new VBox();
		VBox btnBox = new VBox();
		
		VBox personFileBox = new VBox();
		VBox startFileBox = new VBox();
		VBox regBox1 = new VBox();
		VBox regBox2 = new VBox();
		VBox regBox3 = new VBox();
		HBox options = new HBox();
		
		Font buttonFont = new Font(30);
		Font infoFont = new Font("helvetica", 15);
		Font headerFont = new Font("helvetica", 25);

		Text competitionText = new Text("Enter result header");
		TextField inputCompetitionText = new TextField("");



		
		final Button startButton = createButton("Start Times", startFileBox, "Start file", PrimaryStage, 7, 0);
		final Button regButton1 = createButton("Time Registrations 1", regBox1, "Registration file", PrimaryStage, 8, 0);
		final Button nameButton = createButton("Participants Details", personFileBox, "Participant file", PrimaryStage, 6, 0);
		final Button regButton2 = createButton("Time Registrations 2", regBox2, "Registration file", PrimaryStage, 9, 0);
		final Button regButton3 = createButton("Time Registrations 3", regBox3, "Registration file", PrimaryStage, 10, 0);

		final Button unsortedResultButton = new Button("Generate unsorted result");
		unsortedResultButton.setMaxWidth(Double.MAX_VALUE);


		final Button sortedResultButton = new Button("Generate sorted result");
		sortedResultButton.setMaxWidth(Double.MAX_VALUE);

		final RadioButton marathon = new RadioButton("Marathon");
		final RadioButton multipleLaps = new RadioButton("Multiple Laps");

		ToggleGroup radioGroup = new ToggleGroup();

		marathon.setToggleGroup(radioGroup);
		multipleLaps.setToggleGroup(radioGroup);
		marathon.setSelected(true);

		Text mt = new Text("Write the minimal time (hh:mm:ss).");
		Text infoHeader = new Text("In order to create a result file:");
		Text info = new Text(
			"* Enter a description for the competition in the textfield under 'Enter result header'\n" +
			"* Select the type of competition; marathon or multiple laps\n" +
			"* Enter the minimum time for compleating a lap under 'Write the minimal time\n" +
			"* Select a file containing the participant's details using the participant details button\n" +
			"* Select a file with start times using the start time button\n" +
			"* Select at least one file containing time registrations using the one or several of the registration file buttons\n" +
			"* When the above files are selected, click the generate result button and name your file"
		);


		infoHeader.setFont(headerFont);
		headerBox.getChildren().add(infoHeader);
		info.setFont(infoFont);
		infoBox.getChildren().add(info);
		options.getChildren().addAll(marathon, multipleLaps);

		TextField minTF = new TextField("00:15:00");
		minTF.setOnKeyPressed(event -> {
			if (event.getCode().equals(KeyCode.ENTER)) {
				minimitime = minTF.getText();

			}
		});

		unsortedResultButton.setFont(buttonFont);
		sortedResultButton.setFont(buttonFont);

		unsortedResultButton.setOnAction(e -> {
			btnBox.getChildren().clear();
			try {
				anyFile = startFile != null && (regFile1 != null || regFile2 != null || regFile3 != null) && personFile != null;
				unsortedLapResultFile = be.buttonResult(nameButton, PrimaryStage, btnBox, anyFile);
				competitionInfo = inputCompetitionText.getText();
				if (radioGroup.getSelectedToggle() == marathon) {
					marathonSelected = true;
				} else {
					marathonSelected = false;
				}
				ready();
			} catch (Exception ex) {

			}
		});

		sortedResultButton.setOnAction(e -> {
			btnBox.getChildren().clear();
			try {
				anyFile = startFile != null && (regFile1 != null || regFile2 != null || regFile3 != null) && personFile != null;
				sortedLapsResultFile = be.buttonResult(nameButton, PrimaryStage, btnBox, anyFile);
				competitionInfo = inputCompetitionText.getText();
				if (radioGroup.getSelectedToggle() == marathon) {
					marathonSelected = true;
				} else {
					marathonSelected = false;
				}
				ready();
			} catch (Exception ex) {

			}
		});
		
		final GridPane layout = new GridPane();
		layout.setVgap(10);
		layout.setPadding(new Insets(10));

		layout.getChildren().addAll(headerBox, infoBox, startButton, regButton1, nameButton, unsortedResultButton, sortedResultButton, btnBox, startFileBox, regBox1,
		personFileBox, regButton2, regButton3, regBox2, regBox3, minTF, mt, marathon, multipleLaps, competitionText, inputCompetitionText);

		GridPane.setRowIndex(headerBox,0);
		GridPane.setRowIndex(infoBox,1);
		GridPane.setColumnSpan(infoBox, 4);
		GridPane.setRowIndex(competitionText, 2);
		GridPane.setRowIndex(inputCompetitionText, 3);
		GridPane.setRowIndex(marathon, 4);
		GridPane.setRowIndex(mt, 4);
		GridPane.setColumnIndex(mt, 1);
		GridPane.setRowIndex(unsortedResultButton, 11);
		GridPane.setRowIndex(sortedResultButton, 12);
		GridPane.setRowIndex(btnBox, 12);
		GridPane.setColumnIndex(btnBox, 1);
		GridPane.setRowIndex(multipleLaps, 5);
		GridPane.setColumnIndex(multipleLaps, 0);
		GridPane.setColumnIndex(minTF, 1);
		GridPane.setRowIndex(minTF, 5);
		
		final Scene scene = new Scene(layout, 850, 800);

		PrimaryStage.setTitle("Enduro Watch");
		PrimaryStage.setScene(scene);
		PrimaryStage.show();

		final Thread t = new Thread(() -> {
			try {
				work();
			} catch (final InterruptedException e1) {
				e1.printStackTrace();
			}
		});
		t.start();
	}



	private Button createButton(String buttonName, VBox vb, String text, Stage st, int row, int col) {
		Button button = new Button(buttonName);
		Font font = new Font(30);
		button.setFont(font);


		button.setOnAction(e -> {
			vb.getChildren().clear();
			try {
				if (buttonName == "Start Times") {
					startFile = be.buttonAction(button, text + ": ", st, vb);
				} else if (buttonName == "Time Registrations 1") {
					regFile1 = be.buttonAction(button, text + ": ", st, vb);
				} else if (buttonName == "Participants Details") {
					personFile = be.buttonAction(button, text + ": ", st, vb);
				} else if (buttonName == "Time Registrations 2") {
					regFile2 = be.buttonAction(button, text + ": ", st, vb);
				} else if (buttonName == "Time Registrations 3") {
					regFile3 = be.buttonAction(button, text + ": ", st, vb);
				}
			} catch (Exception ex) {
			}
		});

		button.setMaxWidth(Double.MAX_VALUE);
		GridPane.setRowIndex(button, row);
		GridPane.setColumnIndex(button, col);
		GridPane.setRowIndex(vb, row);
		GridPane.setColumnIndex(vb, col + 1);
		vb.setPadding(new Insets(15));

		return button;
	}

}

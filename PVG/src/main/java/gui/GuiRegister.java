package gui;
import java.io.IOException;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class GuiRegister extends Application {
	private String nof;
	private RegisterList reg;
	Alert errorAlert = new Alert(AlertType.INFORMATION);

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage PrimaryStage) throws Exception, IOException {
		PrimaryStage.setTitle("Register");
		GridPane layout = new GridPane();


		// layout
		layout.setVgap(10);
		layout.setPadding(new Insets(10));

		// file
		Text ft = new Text("Write the name of the file below");
		TextField fileName = new TextField("");
		Button fileBtn = new Button("Save file name");

		// registration
		Text rt = new Text("Write the registration number below");
		TextField regNumber = new TextField("");
		Button button = new Button("Register");
		button.setDisable(true);

		reg = new RegisterList();

		fileName.setOnKeyPressed(event -> {
			if (event.getCode().equals(KeyCode.ENTER)) {
				nof = fileName.getText();
				if (nof.equals(""))
					;
				else {
					if (!testForTxt(nof)) {
						nof = nof + ".txt";
					}
					reg.addFile(nof);
					button.setDisable(false);
					fileBtn.setDisable(true);
					regNumber.requestFocus();
					fileName.setDisable(true);
				}

			}
		});

		regNumber.setOnKeyPressed(event -> {
			if (event.getCode().equals(KeyCode.ENTER)) {
				if (regNumber.getText().equals("")){
					reg.addEntry();
				}else{
					if (isNumeric(regNumber.getText())) {
						reg.addEntry(Integer.parseInt(regNumber.getText()));
						regNumber.clear();
					} 
				}
			}
	});

		button.setOnAction(e -> {
			if (regNumber.getText().equals("")){
				reg.addEntry();
			}else{
				if ( isNumeric(regNumber.getText())) {
					reg.addEntry(Integer.parseInt(regNumber.getText()));
					regNumber.clear();
				} else {
					errorMsg();
			}
		}

		});
		fileBtn.setOnAction(e -> {
			nof = fileName.getText();
			if (nof.equals(""))
				;
			else {
				if (!testForTxt(nof)) {
					nof = nof + ".txt";
				}
				reg.addFile(nof);
				button.setDisable(false);
				fileBtn.setDisable(true);
				regNumber.requestFocus();
				fileName.setDisable(true);
			}
		});
		layout.getChildren().addAll(ft, fileName, fileBtn, button, regNumber, rt, reg);
		GridPane.setRowIndex(ft, 0);
		GridPane.setRowIndex(fileName, 1);
		GridPane.setRowIndex(fileBtn, 2);
		GridPane.setRowIndex(rt, 3);
		GridPane.setRowIndex(regNumber, 4);
		GridPane.setRowIndex(button, 5);
		GridPane.setRowIndex(reg, 6);

		Scene scene = new Scene(layout, 300, 400);
		PrimaryStage.setScene(scene);
		PrimaryStage.show();
	}
	public static boolean isNumeric(String str) { 
		try {  
		  Integer.parseInt(str);  
		  return true;
		} catch(NumberFormatException e){  
		  return false;  
		}  
	  }

	private boolean testForTxt(String s) {
		String[] n = s.split("\\.");
		if (n[n.length - 1].equals("txt")) {
			return true;
		}
		return false;
	}

	private void errorMsg() {
		errorAlert.setHeaderText("Information");
		errorAlert.setContentText("Please enter a correct registration number");
		errorAlert.showAndWait();
	}

}

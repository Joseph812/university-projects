package gui;

import java.io.File;
import java.io.IOException;

import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class ButtonExecute {

   private File file;
   private FileChooser fileChooser = new FileChooser();

    public ButtonExecute() {

    }

    public File buttonAction(Button button, String text, final Stage stage, VBox box) throws IOException {
        try{
            file = fileChooser.showOpenDialog(stage);
			Text fileName = new Text(text + file.getName());
			fileName.setFont(new Font(20));
	
            box.getChildren().add(fileName);
        
        }catch(Exception e ){
            System.out.println(e);
        }
        return file;
      
        
    }

    public String buttonResult(Button button, final Stage stage, VBox box, Boolean allFiles) throws IOException {
        
        Text errorMesage = new Text("Please make sure you have selected all files");
        errorMesage.setFont(new Font(15));
		errorMesage.setFill(Color.RED);
        file = fileChooser.showSaveDialog(stage);
        String resultFile = "";
			if(file != null && allFiles) {
				resultFile = file.toPath().toString();
				box.getChildren().remove(errorMesage);
			}	
			else {
				box.getChildren().add(errorMesage);
            }

            return resultFile; 

    }
}
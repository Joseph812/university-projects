package gui;

import java.io.File;

import datastructure.EnduroLocalTime;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import parse.Register;

public class RegisterList extends VBox {
    private Register reg;
    Alert errorAlert = new Alert(AlertType.INFORMATION);

    public RegisterList() {
        super();
    }

    public void addFile(String file) {
        reg = new Register(new File(file));
    }

    public void addEntry(int plac) {
        reg.addEntry(plac, EnduroLocalTime.now().toString());
        this.getChildren().add(new Text(plac + ": " + EnduroLocalTime.now().toString()));
    }

    public void addEntry() {
        HBox hb = new HBox();
        hb.setSpacing(10);
        TextField change = new TextField("No Number");
        change.setMaxWidth(120);
        String time = EnduroLocalTime.now().toString();
        hb.getChildren().add(change);
        hb.getChildren().add(new Text(time));
        this.getChildren().add(hb);
        Button removeB = new Button("cancel");
        hb.getChildren().add(removeB);
        removeB.setOnAction(removeEvent -> {
            this.getChildren().removeAll(hb);
        });
        
        change.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {

            change.clear();
            }
        }
            );

        change.setOnKeyPressed(changeEvent -> {
            if (changeEvent.getCode().equals(KeyCode.ENTER)) {
                if (isNumeric(change.getText())) {
                    this.getChildren().removeAll(hb);
                    reg.addEntry(Integer.parseInt(change.getText()), time);
                    this.getChildren().add(new Text(change.getText() + ": " + time));
                } else {
                    errorMsg();
                }
            }
        });
    }

    private static boolean isNumeric(String str) {
        try {
            Integer.parseInt(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private void errorMsg() {
        errorAlert.setHeaderText("Information");
        errorAlert.setContentText("Please enter a correct registration number");
        errorAlert.showAndWait();
    }
}
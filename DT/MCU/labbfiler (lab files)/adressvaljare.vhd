----------------------------------------------------------------------------------
-- Namn:        adressvaljare
-- Filnamn:     adressvaljare.vhd
-- Testbench:   adressvaljare_tb.vhd
--
-- Insignaler:
--      clk     - klocksignal, all uppdatering av register sker vid stigande flank
--      n_rst   - synkron resetsignal, aktiv låg
--      DATA    - de 6 minst signifikanta bitarna från instruktionen, används då 
--                nästa adress anges av instruktionen
--      AddrSrc - bestämmer varifrån nästa adress ska hämtas
--      StackOp - styr stacken i adressväljaren
--
-- Utsignaler:
--      A           - nästa adress
--      pc_debug    - nuvarande adress, används för att visa adressen på 
--                    Nexys4 display
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.all;
use work.cpu_pkg.all;

entity adressvaljare is
    port(
        clk, n_rst : in std_logic;
        DATA : in std_logic_vector(5 downto 0);
        A : out std_logic_vector(5 downto 0);
        AddrSrc : in std_logic_vector(1 downto 0);
        StackOp : in std_logic_vector(1 downto 0);
        pc_debug : out std_logic_vector(5 downto 0)
    );
end entity;

architecture structural of adressvaljare is
 signal va : std_logic_vector(5 downto 0) := "000000"; -- MUX ut
 signal vb : std_logic_vector(5 downto 0) := "000000"; -- REG ut
 signal vc : std_logic_vector(5 downto 0) := "000000"; -- ToS ut
 signal vd : std_logic_vector(5 downto 0) := "000000"; -- PC+1 storlek 6
 signal ve : std_logic_vector(7 downto 0) := "00000000"; -- PC+1 ut
 signal addTwo : std_logic_vector(7 downto 0) := "00000000";
begin
    
    
    REG6a : entity REG6 port map(
              CLR => n_rst,
              ENA => '1',
              D => va,
              CLK => clk,
              Q => vb 
         );
         
    Stacka : entity stack port map(
    D => DATA,
    StackOp => StackOp,
    clk => clk,
    n_rst => n_rst,
    ToS => vc
    );
    
    addTwo <= "00" & vb;
    
    ALUa : entity ALU8 port map (
    A => addTwo,
    B => "00000001",
    S => "010",
    Z => open,
    F => ve
    );
    
    vd <= ve(5 downto 0);
    
    
    
    MUXa : entity MUX3x6 port map(
     IN0 => vd,
     IN1 => DATA,
     IN2 => vc,
     SEL => AddrSrc,
     O => va
    );
    
    
    
    
    

    A <= va;
    pc_debug <= vb;
end architecture;

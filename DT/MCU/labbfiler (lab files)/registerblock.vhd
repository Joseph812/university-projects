----------------------------------------------------------------------------------
-- Namn:        registerblock
-- Filnamn:     registerblock.vhd
-- Testbench:   registerblock_tb.vhd
--
-- Insignaler:
--      clk     - klocksignal, all uppdatering av register sker vid stigande flank
--      n_rst   - synkron resetsignal, aktiv låg
--      F       - resultatet från ALU
--      DEST    - bestämmer vilket av registerna R0 och R1 som ska vara aktivt
--      RegEna  - laddsignal till det aktiva registret
--
-- Utsignaler:
--      RegOut  - det aktiva registrets innehåll
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.all;
use work.cpu_pkg.all;

entity registerblock is
    port(
        clk : in std_logic;
        n_rst : in std_logic;
        F : in std_logic_vector(7 downto 0);
        DEST : in std_logic;
        RegEna : in std_logic;
        RegOut : out std_logic_vector(7 downto 0)
    );
end entity;

architecture structural of registerblock is
  signal R0UT : std_logic_vector(7 downto 0) := "00000000";
  signal R1UT : std_logic_vector(7 downto 0) := "00000000";
  signal R0IN : std_logic := '0';
  signal R1IN : std_logic := '0';
  
begin

R0IN <= RegEna and (not DEST);

 R0 : entity REG8 port map(
 CLR => n_rst,
 ENA => R0IN, 
 D => F,
 CLK => clk,
 Q => R0UT
 );
 
 R1IN <= RegEna and DEST;
 R1 : entity REG8 port map(
  CLR => n_rst,
  ENA => R1IN, 
  D => F,
  CLK => clk,
  Q => R1UT
  );
  
  MUXa : entity MUX2x8 port map(
  IN0 => R0UT,
  IN1 => R1UT,
  SEL => DEST,
  O => RegOut
  );
end architecture;

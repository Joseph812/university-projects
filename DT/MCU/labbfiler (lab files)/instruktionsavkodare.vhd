----------------------------------------------------------------------------------
-- Namn:        instruktionsavkodare
-- Filnamn:     instruktionsavkodare.vhd
-- Testbench:   instruktionsavkodare_tb.vhd
--
-- Insignaler:
--      OPCODE - operationskod från instruktionen
--      Z      - zero-flagga från beräkningsenhet
--
-- Utsignaler:
--      StackOp - styr stacken i adressväljaren
--      AddrSrc - styr varifrån nästa adress ska hämtas
--      ALUOp   - bestämmer operatinen för ALU i beräkningsenhet
--      ALUSrc  - väljer om ett register eller insignalen från IO-blocket ska 
--                vara operand till ALU
--      RegEna  - laddsignal till registerblocket
--      OutEna  - laddsignal till utsignalsregistret i IO-blocket
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.all;
use work.cpu_pkg.all;

entity instruktionsavkodare is
    port(
        OPCODE : in std_logic_vector(3 downto 0);
        Z : in std_logic;
        StackOp : out std_logic_vector(1 downto 0);
        AddrSrc : out std_logic_vector(1 downto 0);
        ALUOp : out std_logic_vector(2 downto 0);
        ALUSrc : out std_logic;
        RegEna : out std_logic;
        OutEna : out std_logic
    );
end entity;

architecture behaviour of instruktionsavkodare is
     signal RegOut : std_logic := '0';
     signal Input : std_logic := '1';
begin

 
INST_CASE : Process(OPCODE, Z)

begin 

case OPCODE is
    when OPCODE_CALL =>
        StackOP <= STACK_OP_PUSH;
        AddrSrc <= ADDR_DATA;
        OutEna <= '0';
        RegEna <= '0';
    when OPCODE_RET =>
        StackOP <= STACK_OP_POP;
            AddrSrc <= ADDR_TOS;
            OutEna <= '0';
            RegEna <= '0';
    when OPCODE_BZ =>
    if Z = '0' then
        StackOP <= STACK_OP_HOLD;
        AddrSrc <= ADDR_DATA;
        ALUOp <= "110";
        ALUSrc <= RegOut;
        OutEna <= '0';
        RegEna <= '0';
    else 
        StackOP <= STACK_OP_HOLD;
        AddrSrc <= ADDR_PC_PLUS_ONE;
        ALUOp <= "110";
        ALUSrc <= RegOut;
        OutEna <= '0';
        RegEna <= '0';
    end if;
    when OPCODE_B =>
        StackOP <= STACK_OP_HOLD;
        AddrSrc <= ADDR_PC_PLUS_ONE;
        OutEna <= '0';
        RegEna <= '0';
    when OPCODE_ADD =>
        StackOP <= STACK_OP_HOLD;
            AddrSrc <= ADDR_DATA;
            ALUOp <= "010";
            OutEna <= '0';
            RegEna <= '1';
    when OPCODE_SUB =>
        StackOP <= STACK_OP_HOLD;
        AddrSrc <= ADDR_PC_PLUS_ONE;
        ALUOp <= "011";
        ALUSrc <= RegOut;
        OutEna <= '0';
        RegEna <= '1';
    when OPCODE_LD =>
        StackOP <= STACK_OP_HOLD;
        AddrSrc <= ADDR_PC_PLUS_ONE;
        ALUOp <= "000";
        ALUSrc <= RegOut;
        OutEna <= '0';
        RegEna <= '1';
    when OPCODE_IN =>
        StackOP <= STACK_OP_HOLD;
        AddrSrc <= ADDR_PC_PLUS_ONE;
        ALUOp <= "001";
        ALUSrc <= Input;
        OutEna <= '0';
        RegEna <= '1';
    when OPCODE_OUT =>
        StackOP <= STACK_OP_HOLD;
        AddrSrc <= ADDR_PC_PLUS_ONE;
        ALUOp <= "001";
        ALUSrc <= RegOut;
        OutEna <= '1';
        RegEna <= '0';
    when OPCODE_AND =>
        StackOP <= STACK_OP_HOLD;
        AddrSrc <= ADDR_PC_PLUS_ONE;
        ALUOp <= "100";
        ALUSrc <= RegOut;
        OutEna <= '0';
        RegEna <= '1';
    when others =>
end case;

end process;


end architecture;

-------------------------------------------------------------------------------
-- Title      : FSM
-- Project    : 
-------------------------------------------------------------------------------
-- File       : FSM.vhd
-- Author     : Steffen Malkowsky  <Steffen@Steffens-MacBook-Air.local>
-- Company    : 
-- Created    : 2014-06-14
-- Last update: 2014-07-19
-- Platform   : 
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: implementation of the FSM used to control the stop watch.
-------------------------------------------------------------------------------
-- Copyright (c) 2014 Lund University
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2014-06-14  1.0      Steffen Created
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity FSM is
  
  port (
    clk     : in  std_logic;
    n_rst   : in  std_logic;
    start   : in  std_logic;
    stop    : in  std_logic;
    run_cnt : out std_logic;
    set0    : out std_logic);

end entity FSM;

architecture FSM_arch of FSM is

  -----------------------------------------------------------------------------
  -- TODO:
  -----------------------------------------------------------------------------
  type state_type is (q0, q1);                -- Add your states of the FSM
  signal  current_state, next_state : state_type;
  
begin  -- architecture FSM_arch
    --if current_state = q0 and start = '1' and stop = '0' then
     --   current_state <= q1;
    --else  
      --  current_state <= q0;
        
        
        
  reg : process (clk, n_rst) is
  begin  
    ---------------------------------------------------------------------------
    -- TODO:
    -- Implement the register holding the current_state using signals
    -- current_state and next_state 
    ---------------------------------------------------------------------------
    if n_rst = '1' then
        if clk = '1' then
            current_state <= next_state;
           -- else
           -- next_state <= current_state;
        end if;
        else current_state <= q0;
        end if;
  end process reg;

  comb : process (current_state, start, stop) is
  --variable v_inputs : std_logic_vector(2 downto 0);
  begin  
    ---------------------------------------------------------------------------
    -- TODO:
    -- Implement your transition logic based on your FSM graph and set the
    -- outputs.
    ---------------------------------------------------------------------------
   
    --next_state <= (current_state and start) or (start and (not stop)) or (current_state and (not stop));

 --   case current_state is
 --       when q0 => v_inputs := '0' & start & stop;
 --       when q1 => v_inputs := '1' & start & stop;
 --   end case
    
    case current_state is
            when q0 => 
--if current_state = q0 then
                if start = '1' and stop = '0' then
                    next_state <= q1;
                    run_cnt <= '1';
                    set0 <= '0';
                elsif start = '0' and stop = '1' then
                    next_state <= q0;
                    run_cnt <= '0';
                    set0 <= '1';
                else 
                    next_state <= q0;
                    run_cnt <= '0';
                    set0 <= '0';
                end if;
            when q1 =>
--else
                if start = '1' or stop = '0' then
                    next_state <= q1;
                    run_cnt <= '1';
                    set0 <= '0';
                else 
                next_state <= q0;
                run_cnt <= '0';
                set0 <= '0';
                end if;
     end case;
--        end if;
        
    
--    case v_inputs is
--        when "110" | "111" | "010"  | "100" => 
--            next_state <= q1;
--        when others => 
--            next_state <= q0;
--        end case;

  end process comb;
    

  end architecture FSM_arch;

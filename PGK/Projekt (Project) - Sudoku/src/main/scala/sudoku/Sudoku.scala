package sudoku

class Sudoku(var v: Vector[Vector[Cell]]) {
  val dim: (Int, Int) = (9, 9)

  var origSudoku: Vector[Vector[Cell]] = Vector.empty

  def update(r: Int, c: Int)(n: Cell): Unit = v = v.updated(r, v(r).updated(c, n))

  def remove(r: Int, c:Int): Unit = v = v.updated(r, v(r).updated(c, Cell.empty))

  def apply(r: Int, c: Int): Cell = v(r)(c)

  def addRow(r: Int)(cv: Vector[Cell]): Unit = v = v.updated(r, cv)

  def removeRow(r: Int): Unit = v = v.updated(r, Vector(Cell.empty, Cell.empty, Cell.empty, Cell.empty, Cell.empty, Cell.empty, Cell.empty, Cell.empty, Cell.empty))

  def count(n: Cell): Int = {
    var m = 0
    for (x <- 0 until 9){
      if (v(x).contains(n)){
        m += 1
      }
    }
    m
  }

  def isOccupied(r: Int, c: Int): Boolean = apply(r, c).value != 0

  def isFull: Boolean = v.map(_.forall(_.value != 0)).forall(_ == true)

  def smallMidPos(srow: Int, scol: Int): (Int, Int) = {
    val smallrow = srow match{
      case 0 => 1; case 1 => 1; case 2 => 1
      case 3 => 4; case 4 => 4; case 5 => 4
      case 6 => 7; case 7 => 7; case 8 => 7
    }
    val smallcol = scol match{
      case 0 => 1; case 1 => 1; case 2 => 1
      case 3 => 4; case 4 => 4; case 5 => 4
      case 6 => 7; case 7 => 7; case 8 => 7
    }
    (smallrow, smallcol)
  } //från vanlig till field (mittpunkt av fälten som pos befinner sig i)

  def smallField(r: Int, c: Int): Vector[Vector[Cell]] = {                             // ger fälten av midpunkten MidPos
    val s = smallMidPos(r, c)
    if (s._2 == 7){
      Vector(v(s._1 - 1).slice(s._2-1, s._2+1) :+ v(s._1 - 1)(8), v(s._1).slice(s._2-1, s._2+1) :+ v(s._1)(8), v(s._1 + 1).slice(s._2-1, s._2+1) :+ v(s._1 + 1)(8))
    } else Vector(v(s._1 - 1).slice(s._2-1, s._2+2), v(s._1).slice(s._2-1, s._2+2), v(s._1 + 1).slice(s._2-1, s._2+2))
  }


  def bigMidPos(brow: Int, bcol: Int): (Int, Int) = { // från index av field till vanlig mid punkt
    val bigrow = brow match {
      case  1 => 1
      case  2 => 4
      case  3 => 7
    }
    val bigcol = bcol match {
      case 1 => 1
      case 2 => 4
      case 3 => 7
    }
    (bigrow, bigcol)
  }

  def bigField(r: Int, c: Int): Vector[Vector[Cell]] = {                             // ger fälten av midpunkten fieldMidPos
    val s = bigMidPos(r, c)
    if (s._2 == 7){
      Vector(v(s._1 - 1).slice(s._2-1, s._2+1) :+ v(s._1 - 1)(8), v(s._1).slice(s._2-1, s._2+1) :+ v(s._1)(8), v(s._1 + 1).slice(s._2-1, s._2+1) :+ v(s._1 + 1)(8))
    } else Vector(v(s._1 - 1).slice(s._2-1, s._2+2), v(s._1).slice(s._2-1, s._2+2), v(s._1+1).slice(s._2-1, s._2+2))
  }

  override def toString:String = v.map(x => x.map(_.value).mkString("")).mkString("\n")
}
// ---------------------------------------------------------------------------------------------------------------------
object Sudoku {

  def randomPos(): (Int, Int)= ((Math.random*9).toInt, (Math.random*9).toInt)

  def diff(s1: Sudoku, s2: Sudoku): Sudoku = {
    val r1 = s1.v(0) diff s2.v(0)
    val r2 = s1.v(1) diff s2.v(1)
    val r3 = s1.v(2) diff s2.v(2)
    val r4 = s1.v(3) diff s2.v(3)
    val r5 = s1.v(4) diff s2.v(4)
    val r6 = s1.v(5) diff s2.v(5)
    val r7 = s1.v(6) diff s2.v(6)
    val r8 = s1.v(7) diff s2.v(7)
    val r9 = s1.v(8) diff s2.v(8)
    new Sudoku(Vector(r1, r2, r3, r4, r5, r6, r7, r8, r9))
  }

  def empty = new Sudoku(Vector.fill(9, 9)(Cell.empty))

  def defaultRules(r: Int, c: Int, s: Sudoku, cell: Cell): Boolean = {

    val xy = s.smallField(r, c)                   //field matris av dim 3x3 som innehåller värdet i (r, c)
    val xs: Vector[Cell] = {                      //Vector som innehåller alla värden i field
      var x: Vector[Cell] = Vector.empty
      for(i <- xy.indices){
        x = x ++ xy(i)
      }
      x
    }

    if(!(s.v(r) contains cell) && !(s.v.map(x => x(c)) contains cell) && !(xs contains cell) && !s.isOccupied(r, c)) {
      true
    } else false
  }

  def randomLoad(): Sudoku ={
    val randomSudokuStart = (Math.random()*50).toInt*9
    val s = scala.io.Source.fromFile("SudokuLibrary.txt", "UTF-8").getLines.mkString("\n").replaceAllLiterally(" ", "")
    val all: Vector[String] = s.split("\n").toVector
    val onlySudoku = all.slice(randomSudokuStart, randomSudokuStart+10)
    var v1: Vector[Vector[Cell]] = Vector.empty
    for(j <- 0 to 8){
      var v2: Vector[Cell] = Vector.empty
      for(i <- 0 to 8){
        val value = onlySudoku(j) apply i
        v2 = v2 :+ Cell.toCell(value.toInt - 48)
      }
      v1 = v1 :+ v2
    }
    new Sudoku(v1)
  }

  def randomVector(): Vector[Int] = {
    var v = Vector(1, 2, 3, 4, 5, 6, 7, 8, 9)
    var v2: Vector[Int] = Vector.empty
    while (v.nonEmpty){
      val x = v((math.random*v.size).toInt)
      v = v.filter(_ != x)
      v2 = v2 :+ x
    }
    v2
  }

  def vectorRule(r: Int, vec: Vector[Cell], g: Sudoku): Boolean = {
    vec.forall(x => defaultRules(r, vec.indexOf(x), g, x))
  }

    def generateRec(row: Int, col: Int, g: Sudoku): Boolean = {
    if(row == 9){
      return true
    }

    else if (row >= 5){
       for(i <- 1 until 10){
         if(defaultRules(row, col, g, Cell.toCell(i))){
           g.update(row, col)(Cell.toCell(i))
           if (col == 8){
            if(generateRec(row + 1,0, g)){
             return true
             }
           } else if (generateRec(row, col + 1, g)){
             return true
           }
           g.remove(row, col)
         }
       }
      return false
   }

     var v2 = Cell.toVecCell(randomVector())
     if(vectorRule(row, v2, g)) {
       g.addRow(row)(v2)
       while (!generateRec(row + 1, col,  g)) {
         g.removeRow(row)
         v2 = Cell.toVecCell(randomVector())
         while(!vectorRule(row, v2, g)) {
           v2 = Cell.toVecCell(randomVector())
         }
         g.addRow(row)(v2)
       }
       return true
   }
   false
 }

    def generate(n: Int): Sudoku = {
      val g = empty

      generateRec(0 ,0, g)
      for (_ <- 0 until n) {
        var r = randomPos()
        while(!g.isOccupied(r._1, r._2)){
          r = randomPos()
        }
        g.update(r._1, r._2)(Cell.empty)
      }
      g.origSudoku = g.v
      g
    }

  def solveSudo(row: Int, col: Int, g: Sudoku): Boolean ={
    if (row == 9){
      return true
    }
    if(g.isOccupied(row, col)){
      if(col == 8){
        solveSudo(row + 1, 0, g)
      } else solveSudo(row, col + 1, g)
    } else {
      for (i <- 1 to 9) {
        if (defaultRules(row, col, g, Cell.toCell(i))) {
          g.update(row, col)(Cell.toCell(i))
          if (col == 8) {
            if (generateRec(row + 1, 0, g)) {
              return true
            }
          } else if (generateRec(row, col + 1, g)) {
            return true
          }
          g.remove(row, col)
        }
      }
      return false
    }
  }
}


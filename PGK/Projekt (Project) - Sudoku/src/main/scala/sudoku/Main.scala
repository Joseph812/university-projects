package sudoku
import scala.util.Try
object Main {
  var f = ""

  def fromString(s: String, rowDelim: String="\n"): Unit = {

    val vecS = s.split(rowDelim)
    val loadedlvl = vecS(0).toInt
    loadedlvl match {
      case 1 => {
        val ES2 = new EasySG
        ES2.time = vecS(1).toDouble
        ES2.rightNbrs = vecS(2).toInt
        val onlyOrigSudoku = vecS.slice(3, 12).mkString("\n")                         //changed
        val onlyAnswSudoku = vecS.slice(13, vecS.size).mkString("\n")                  //changed
        val a: Vector[String] = onlyOrigSudoku.split(rowDelim).toVector               //changed
        val b: Vector[String] = onlyAnswSudoku.split(rowDelim).toVector                 //changed
  //      println("a = " + a)
        var v1: Vector[Vector[Cell]] = Vector.empty
        var x1: Vector[Vector[Cell]] = Vector.empty
        for(j <- 0 to 8){
          var v2: Vector[Cell] = Vector.empty
          var x2: Vector[Cell] = Vector.empty
          for(i <- 0 to 8){
            val value = a(i) apply j
            v2 = v2 :+ Cell.toCell(value.toInt - 48)
            val valueX = b(i) apply j
            x2 = x2 :+ Cell.toCell(valueX.toInt - 48)
          }
          v1 = v1 :+ v2
          x1 = x1 :+ x2
        }
        ES2.sudoku.v = v1
        ES2.sudoku.origSudoku = v1
        ES2.addAnswers(x1)
   //     println(ES2.sudoku.toString)
        ES2.play()
      }
      case 2 => {
        val MS2 = new MediumSG
        MS2.time = vecS(1).toDouble
        MS2.rightNbrs = vecS(2).toInt
        val onlyOrigSudoku = vecS.slice(3, 12).mkString("\n")                         //changed
        val onlyAnswSudoku = vecS.slice(13, vecS.size).mkString("\n")                  //changed
        val a: Vector[String] = onlyOrigSudoku.split(rowDelim).toVector               //changed
        val b: Vector[String] = onlyAnswSudoku.split(rowDelim).toVector                 //changed
        //      println("a = " + a)
        var v1: Vector[Vector[Cell]] = Vector.empty
        var x1: Vector[Vector[Cell]] = Vector.empty
        for(j <- 0 to 8){
          var v2: Vector[Cell] = Vector.empty
          var x2: Vector[Cell] = Vector.empty
          for(i <- 0 to 8){
            val value = a(i) apply j
            v2 = v2 :+ Cell.toCell(value.toInt - 48)
            val valueX = b(i) apply j
            x2 = x2 :+ Cell.toCell(valueX.toInt - 48)
          }
          v1 = v1 :+ v2
          x1 = x1 :+ x2
        }
        MS2.sudoku.v = v1
        MS2.sudoku.origSudoku = v1
        MS2.addAnswers(x1)
    //    println(MS2.sudoku.toString)
        MS2.play()
      }
      case 3 => {
        val HS2 = new HardSG
        HS2.time = vecS(1).toDouble
        HS2.rightNbrs = vecS(2).toInt
        val onlyOrigSudoku = vecS.slice(3, 12).mkString("\n")                         //changed
        val onlyAnswSudoku = vecS.slice(13, vecS.size).mkString("\n")                  //changed
        val a: Vector[String] = onlyOrigSudoku.split(rowDelim).toVector               //changed
        val b: Vector[String] = onlyAnswSudoku.split(rowDelim).toVector                 //changed
        //      println("a = " + a)
        var v1: Vector[Vector[Cell]] = Vector.empty
        var x1: Vector[Vector[Cell]] = Vector.empty
        for(j <- 0 to 8){
          var v2: Vector[Cell] = Vector.empty
          var x2: Vector[Cell] = Vector.empty
          for(i <- 0 to 8){
            val value = a(i) apply j
            v2 = v2 :+ Cell.toCell(value.toInt - 48)
            val valueX = b(i) apply j
            x2 = x2 :+ Cell.toCell(valueX.toInt - 48)
          }
          v1 = v1 :+ v2
          x1 = x1 :+ x2
        }
        HS2.sudoku.v = v1
        HS2.sudoku.origSudoku = v1
        HS2.addAnswers(x1)

   //     println(HS2.sudoku.toString)
        HS2.play()
      }
    }
  }

  def fromScoreString(): String = {
    val s = scala.io.Source.fromFile("Highscore.txt", "UTF-8").getLines().map(_.split(",").toVector.reverse.mkString("           ")).slice(0, 10).mkString("\n")
    "Player ID      Score\n" + s
  }

  val buttons1: Seq[String] = Seq("New Game", "Load Game", "Score Board", "Quit").reverse

  val buttons2: Seq[String] = Seq("Easy", "Normal", "Hard", "Completed", "Return").reverse

  def runS1(): Any ={
    val selected1 = introprog.Dialog.select("Start Menu.", buttons1, "Sudoku")
    selected1 match {
      case "New Game" => runS2()
      case "Load Game" => {
        Try(f = scala.io.Source.fromFile(introprog.Dialog.file("Load", "./Desktop/pgk/w13/lab/sudoku"), "UTF-8").getLines.mkString("\n")).getOrElse(runS1())
        fromString(f)
      }
      case "Score Board" => if(introprog.Dialog.isOK(fromScoreString())) runS1() else runS1()
      case "Quit"       => {
        val s = introprog.Dialog.isOK("Are you sure?")
        if(s) println("Exiting main... Goodbye!") else runS1()
      }
      case _            => println("Exiting main... Goodbye!")
    }
  }

  def runS2(): Any = {
    val selected2 = introprog.Dialog.select("Choose Difficulty.", buttons2, "Sudoku")
    selected2 match {
      case "Easy"       => (new EasySG).play()
      case "Normal"     => (new MediumSG).play()
      case "Hard"       => (new HardSG).play()
      case "Completed"  => {
        Sudoku.solveSudo( 0, 0, Sudoku.generate(35))
        return runS1()
      }
      case "Return"     => return runS1()
      case _            => println("Exiting main... Goodbye!")
    }
  }


  def main(args: Array[String]): Unit = {
 //   try {
      runS1()
  //  } catch e => println(s"Something Went Wrong: $e")
  }
}

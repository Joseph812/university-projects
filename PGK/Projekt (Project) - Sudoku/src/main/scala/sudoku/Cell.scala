package sudoku
//import scala.util.{Try, Success, Failure}

case class Cell(v: Option[Int]) {
  val value: Int = v match {
    case Some(x) => x
    case _ => 0
  }

  def isEmpty: Boolean = value == 0
}

object Cell {

  def apply(c: Cell): Int = c.value

  def toCell(x: Int): Cell = Cell(Option(x))

  def toVecCell(v: Vector[Int]): Vector[Cell] = v.map(x => toCell(x))

  def empty = new Cell(None)

  def random(): Cell = Cell(Option((Math.random*9 + 1).toInt))
}

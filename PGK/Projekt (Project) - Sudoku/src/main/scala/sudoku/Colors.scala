package sudoku

object Colors { // välj fina färger
  import java.awt.Color
  val Grid: java.awt.Color        = Color.white
  val WrongNumber: java.awt.Color      = Color.red.brighter
  val Text: java.awt.Color        = Color.white
  val Red: java.awt.Color         = Color.red
  val DarkGreen: java.awt.Color   = new Color(10,255,128).darker
  val Shade: java.awt.Color       = Color.green.brighter
  val GameOver: java.awt.Color    = Color.red.darker()
  val LightBlue: java.awt.Color        = new Color(10,128,255).brighter
  val DarkBlue: java.awt.Color    = new Color(10,128,255).darker
  val Background: java.awt.Color  = Color.black
}

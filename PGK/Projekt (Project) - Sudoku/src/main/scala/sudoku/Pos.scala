package sudoku

case class Pos(x: Int, y: Int) {
  def moved(delta: (Int, Int)): Pos = {
    val b = (delta._1, delta._2)
    Pos(x + b._1, y + b._2)
  }
}

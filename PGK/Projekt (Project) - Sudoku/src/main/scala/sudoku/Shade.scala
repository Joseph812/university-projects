package sudoku

 class Shade(val name: String,
                     var pos: Pos,
                     var dir: (Int, Int),
                     val keyControl: KeyControl,
                     val color: java.awt.Color = Colors.Shade
                    ){

  def setDir(key: String): Unit = {
    if (keyControl.has(key)) dir = keyControl.direction(key)
  }

  def move(): Unit = {
    pos = nextPos
  }

  def nextPos: Pos = {
    pos.moved(dir._1*60, dir._2*60)
  }
}

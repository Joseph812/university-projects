package sudoku

final case class HardSG() extends SudokuGame("Hard Sudoku Game") {

  override val lvl = 3

  val randomNumber: Int = (Math.random*7).toInt + 55

  override val sudoku: Sudoku = Sudoku.generate(randomNumber)

  override val s = new Sudoku(sudoku.origSudoku)

  override def isGameOver(): Boolean = sudoku.isFull

  override var time: Double = 0.0

  override def updateTime(): Unit = {
    time = time + 0.033333333333333333333333
    drawTextInMessageArea(s"Time: ${time.toInt} s", 1, 1, Colors.Text, 15)
    //    println(time)
  }

  var points = 0

  override def updateScore(): Unit= {
    points = rightNbrs*500 - (time/2).toInt
    clearMessageArea()
    pixelWindow.drawText(s"Points: $points", blockSize, dim._2*blockSize + blockSize, Colors.Text, 15)
    pixelWindow.drawText("W, A, S, D to move. Press Q to save your progress." , blockSize - blockSize/3, dim._2*blockSize + 2*blockSize, Colors.Text, 15)
    pixelWindow.drawText("Red number means wrong placement." , blockSize - blockSize/3, dim._2*blockSize + 5*blockSize/2, Colors.Text, 15)
    pixelWindow.drawText("Blue number doesn't have to mean is correct placement but it is good." , blockSize - blockSize/3, dim._2*blockSize + 3*blockSize + blockSize/6, Colors.Text, 11)
    pixelWindow.drawText("Starting numbers cannot be changed." , blockSize - blockSize/3, dim._2*blockSize + 7*blockSize/2 , Colors.Text, 15)
    pixelWindow.drawText( "Enjoy!" , blockSize - blockSize/3, dim._2*blockSize + 4*blockSize, Colors.Text, 15)

  }

  var t = 0
  override var p: Int = 0

  override def scoreWinner(): Unit = {
    t = time.toInt
    p = points
    pixelWindow.drawText(s"Time: $t s", blockSize, dim._2*blockSize + 3*blockSize/2, Colors.Text, 15)
    pixelWindow.drawText(s"Points: $p", blockSize, dim._2*blockSize + blockSize, Colors.Text, 15)
  }

  override def play(): Unit = {
    startGameLoop()
  }
}
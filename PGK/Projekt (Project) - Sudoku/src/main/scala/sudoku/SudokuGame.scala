package sudoku

import introprog.BlockGame
import java.nio.file.{Path, Paths, Files}
import java.nio.charset.StandardCharsets.UTF_8

abstract class SudokuGame(title: String) extends BlockGame(title, (18,18), 30, Colors.Background, 30, 5) {

  val shade = new Shade("Player", Pos(dim._1*blockSize/2 , dim._2*blockSize - messageAreaHeight*blockSize - 4*blockSize), (0, 0), KeyControl("A", "D", "W", "S"))
  val sudoku: Sudoku
  var rightNbrs = 0
  var time: Double

  def drawShade(): Unit = {
    val r = shade.pos.x
    val c = shade.pos.y
    pixelWindow.line(r - (blockSize-4), c - (blockSize-4), r - (blockSize-4), c + (blockSize-4), shade.color, 2)
    pixelWindow.line(r - (blockSize-4), c - (blockSize-4), r + (blockSize-4), c - (blockSize-4), shade.color, 2)
    pixelWindow.line(r + (blockSize-4), c + (blockSize-4), r + (blockSize-4), c - (blockSize-4), shade.color, 2)
    pixelWindow.line(r + (blockSize-4), c + (blockSize-4), r - (blockSize-4), c + (blockSize-4), shade.color, 2)
  }

  def removeOldShade(): Unit ={
    val r2 = shade.pos.x
    val c2 = shade.pos.y
    pixelWindow.line(r2 - (blockSize-4), c2 - (blockSize-4), r2 - (blockSize-4), c2 + (blockSize-4), Colors.Background, 2)
    pixelWindow.line(r2 - (blockSize-4), c2 - (blockSize-4), r2 + (blockSize-4), c2 - (blockSize-4), Colors.Background, 2)
    pixelWindow.line(r2 + (blockSize-4), c2 + (blockSize-4), r2 + (blockSize-4), c2 - (blockSize-4), Colors.Background, 2)
    pixelWindow.line(r2 + (blockSize-4), c2 + (blockSize-4), r2 - (blockSize-4), c2 + (blockSize-4), Colors.Background, 2)

  }

  val s: Sudoku

  def drawNbr(Nbr: Int): Unit = {
    if (!s.isOccupied((shade.pos.x - blockSize / 2) / (blockSize * 2), (shade.pos.y - blockSize / 2) / (blockSize * 2)) && sudoku.apply((shade.pos.x - blockSize / 2) / (blockSize * 2), (shade.pos.y - blockSize / 2) / (blockSize * 2)).value != Nbr) {
      removeNbr()
      if (Sudoku.defaultRules((shade.pos.x - blockSize / 2) / (blockSize * 2), (shade.pos.y - blockSize / 2) / (blockSize * 2), sudoku, Cell.toCell(Nbr))) {
        pixelWindow.drawText(Nbr.toString, shade.pos.x - (blockSize / 4), shade.pos.y - (blockSize / 2), Colors.LightBlue, 30)
        sudoku.update((shade.pos.x - blockSize / 2) / (blockSize * 2), (shade.pos.y - blockSize / 2) / (blockSize * 2))(Cell.toCell(Nbr))
        rightNbrs += 1
      }
      else if(sudoku.apply((shade.pos.x - blockSize / 2) / (blockSize * 2), (shade.pos.y - blockSize / 2) / (blockSize * 2)).value == Nbr) {}
      else {pixelWindow.drawText(Nbr.toString, shade.pos.x - (blockSize / 4), shade.pos.y - (blockSize / 2), Colors.WrongNumber, 30); rightNbrs -= 1}
    }
  }

  def removeNbr(): Unit = {
    pixelWindow.fill(shade.pos.x-(blockSize/4), shade.pos.y-(blockSize/2), 30, 30, Colors.Background)
    sudoku.update((shade.pos.x - blockSize / 2) / (blockSize * 2), (shade.pos.y - blockSize / 2) / (blockSize * 2))(Cell.toCell(0))
  }

  def drawDefaultNbrs(): Unit = {
    for(r <- 0 until 9; c <- 0 until 9) {
        if (sudoku.v(r)(c).value != 0){
          pixelWindow.drawText(s"${sudoku.v(r)(c).value}", r*2*blockSize+blockSize*2/3, c*2*blockSize+blockSize/2, Colors.Text, 30)
        }
    }
  }

  def drawGrid(): Unit = {
    for (x <- 0 to 10) {//vertical lines
      if(Vector(3, 6, 9) contains x)
      pixelWindow.line(x*2*blockSize, 0, x*2*blockSize, dim._1*blockSize, Colors.Red, 3)
      else
        pixelWindow.line(x*2*blockSize, 0, x*2*blockSize, dim._1*blockSize, Colors.Grid)
    }
    for (y <- 0 to 9) {            //horizontal lines
      if(Vector(3, 6, 9) contains y)
        pixelWindow.line(0, y*2*blockSize, dim._2*blockSize, y*2*blockSize, Colors.Red, 3)
      else
      pixelWindow.line(0, y*2*blockSize, dim._2*blockSize, y*2*blockSize, Colors.Grid)
    }
  }

  val lvl: Int

  def saveProgress(): Unit  = {
    pixelWindow.hide()
    val id = introprog.Dialog.input("Name your file.")
    val currentTime = time
    val currentCNbr = rightNbrs
    val currentlvl = lvl
    val currentsudoku = sudoku
    val originalSudoku = new Sudoku(sudoku.origSudoku)
    val stringOS = originalSudoku.toString
    val answeredSudoku = Sudoku.diff(originalSudoku, sudoku)
    val stringAS = answeredSudoku.toString
 //   if(introprog.Dialog.isOK(s"Saved as $id")) Main.runS1() else Main.runS1()
    val data = s"$currentlvl\n$currentTime\n$currentCNbr\n$stringOS\n$stringAS"
    id match {
      case "" =>    Main.fromString(data)
      case _ =>     Files.write(Paths.get(id + ".txt"), data.getBytes(UTF_8)); enterQuittingState()
    }
  }

  def handleKey(sh: Shade, k: KeyControl, key: String): Unit = {
        key.toUpperCase match {
          case k.left   => if (!(shade.nextPos.x < 0)) {
            sh.dir = k.direction(k.left) ; removeOldShade(); sh.move()
          }
          case k.right  => if (!(shade.nextPos.x > dim._1*blockSize)) {
            sh.dir = k.direction(k.right); removeOldShade(); sh.move()
          }
          case k.down   => if (!(shade.nextPos.y >= (dim._2*blockSize))) {
            sh.dir = k.direction(k.down) ; removeOldShade(); sh.move()
          }
          case k.up     => if (!(shade.nextPos.y < 0)) {
            sh.dir = k.direction(k.up)   ; removeOldShade(); sh.move()
          }

          case "1"      => drawNbr(1)
          case "2"      => drawNbr(2)
          case "3"      => drawNbr(3)
          case "4"      => drawNbr(4)
          case "5"      => drawNbr(5)
          case "6"      => drawNbr(6)
          case "7"      => drawNbr(7)
          case "8"      => drawNbr(8)
          case "9"      => drawNbr(9)
          case "0" if !s.isOccupied((shade.pos.x - blockSize/2)/(blockSize*2), (shade.pos.y - blockSize/2)/ (blockSize*2))
                        => removeNbr(); rightNbrs -= 1

          case "Q"      => saveProgress()

        //  case "R"      => Sudoku.solveSudo(0, 0, )

          case _        =>
        }
      }

  var p: Int

  def addHighscore(): Unit = {
    val id = (Math.random()*100000).toInt
    val data = Vector(p, id)
    val s = scala.io.Source.fromFile("Highscore.txt", "UTF-8").getLines().toVector
    val s2 = s.map(_.split(",").toVector.map(_.toInt))
    val newS = (s2 :+ data).sortBy(j => j(0))                   //Vector(Vector(String, String)) sorted
    val newsString = newS.reverse.map(_.mkString(",")).mkString("\n")
    Files.write(Paths.get("Highscore.txt"), newsString.getBytes(UTF_8))
    introprog.Dialog.show(s"Score saved as player ID $id")
  }

  def drawAnswer(Nbr: Int, pos: (Int, Int)): Unit = {
    pixelWindow.drawText(Nbr.toString, pos._1*2*blockSize - blockSize - (blockSize/4), pos._2*2*blockSize-blockSize-(blockSize/2), Colors.LightBlue, 30)
  }

  def addAnswers(Vec: Vector[Vector[Cell]]): Unit = {
    for(j <- 0 until 9){
      for (i <- 0 until 9){
        val value = Vec(j)(i)
        if (value != Cell.toCell(0)){
          sudoku.update(j, i)(value)
          drawAnswer(value.value, (j, i))
        }
      }
    }
  }

  sealed trait State
  case object Starting extends State
  case object Playing  extends State
  case object GameOver extends State
//  case object Pause    extends State
  case object Quitting extends State

  var state: State = Starting

  def enterStartingState(): Unit = {
        clearWindow()
        drawTextInMessageArea(msg = "Press SPACE to start.", x = 1, y = 1, Colors.Text, 20)
        state = Starting
      } // rensa, meddela "tryck space för start"

  def enterPlayingState(): Unit = {
        clearWindow()
        drawGrid()
        drawDefaultNbrs()
        state = Playing
      } // rensa, rita alla entiteter

  def scoreWinner(): Unit

  def enterGameOverState(): Unit = {
        clearMessageArea()
        drawTextInMessageArea("Congratulations! You solved it!", 2, 2, Colors.GameOver, 25)
        scoreWinner()
        addHighscore()
        rightNbrs = 0
        state = GameOver
      }

/*
  def enterPauseState(): Unit = {
    pixelWindow.hide()
    state = Pause

  }
*/

  def enterQuittingState(): Unit = {
        pixelWindow.hide()
        Main.runS1()
        state = Quitting
      }

  override def onKeyDown(key: String): Unit = {
        println(s"""Key "$key" is pressed""")
        state match {
          case Starting => if (key == " ") enterPlayingState()
          case Playing => handleKey(shade, shade.keyControl, key)
          case GameOver => if(key == "Escape") enterQuittingState()
          case _ =>
        }
      }

  override def onClose(): Unit = enterQuittingState()

  def isGameOver(): Boolean

  def updateTime(): Unit

  def updateScore(): Unit

  override def gameLoopAction(): Unit = {
    if (state == Playing) {
      drawShade()
      clearMessageArea()
      updateScore()
      updateTime()
      if (isGameOver()) enterGameOverState()
    }
  }

  def startGameLoop(): Unit = {
    pixelWindow.show()  // möjliggör omstart även om fönstret stängts...
    enterStartingState()
    gameLoop(stopWhen = state == Quitting)
  }

  def play(): Unit // abstrakt, implementeras i subklass
  }

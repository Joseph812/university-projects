package game

object Main {
  def play(): Any = {
    var playAgain = true
    while (playAgain) {
      val game = new Game()
      playAgain = game.run()
    }
    if(!playAgain){
      val s = scala.io.StdIn.readLine("Vill du spela igen?(j/n)\nTryck (s) för att visa Scores")
      if (s == "j") {
        playAgain = true
        play()
      } else if (s == "s"){
        Disk.loadAllScores()
      }
    }
  }

  def main(args: Array[String]): Unit = {
    play()

  }
}

package game
import java.util.Scanner
/** Handles read/write from/to Disk.
  * Reading uses java.util.Scanner
  * Writing uses java.io
  */
object Disk {

  def loadAllScores() = {
  //  println("Scores:\n" + new Scanner("Scores"))
  }

  def appendResult(playerName: String, points: Int, time: Int, fileName: String) = {
    import java.nio.file.{ Paths, Files}
    import java.nio.charset.StandardCharsets.UTF_8
    val data = (playerName, points, time + " Sekunder").toString
    val sc = new Scanner(fileName)
    val s = ""
    while(sc.hasNext){
      s + sc.next
    }
    println(s)
  //  Files.write(Paths.get(fileName), (s + "\n" + data).getBytes(UTF_8))
  }

  /** Save `text` to a text file called `fileName`. */
  private def saveString(text: String, fileName: String): Unit = {
    val f = new java.io.File(fileName)
    val pw = new java.io.PrintWriter(f, "UTF-8")
    try pw.write(text) finally pw.close()
  }
 // saveString("Scores", "Scores.txt")
}

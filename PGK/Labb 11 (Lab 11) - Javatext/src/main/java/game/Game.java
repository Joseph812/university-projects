package game;
import java.util.Scanner;

public class Game {
    /** Start this game. Returns true if player wants to play a new game. */
    int time;
    String name = "";
    public String makeName(){
        Scanner scan = new Scanner(System.in);
        System.out.println("Vad är ditt namn?");
        name = scan.next();
        return name;
    }
    public boolean run(){
        System.out.println("Välkommen till Hangman!");
        makeName();
        Hangman hm = new Hangman();
        String runeberg = "http://runeberg.org/words/ord.ortsnamn.posten";
        int t0 = Math.toIntExact(System.currentTimeMillis() / 1000);
        hm.play(Hangman.download(runeberg, "ISO-8859-1"));
        time = Math.toIntExact(System.currentTimeMillis() / 1000) - t0;
        System.out.println("Tid: " + time + " Sekunder");
        Disk.appendResult(name, hm.points, time, "Scores");

        return false;
    }
}

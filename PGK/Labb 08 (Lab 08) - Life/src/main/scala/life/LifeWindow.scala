package life

import introprog.PixelWindow
import introprog.PixelWindow.Event
import life.Life.{empty, random}


object LifeWindow {
  import java.awt.Color
  val EventMaxWait = 1 // milliseconds
  var NextGenerationDelay = 200 // milliseconds
  val White = new Color(255, 255, 255)
  val MrGreen = new Color(150, 255, 150)
  val Darkred = new Color(139, 0, 0)
  val Black = new Color(0, 0, 0)
  // lägg till fler användbara konstanter här tex färger etc.
}

class LifeWindow(rows: Int, cols: Int){
  import LifeWindow._ // importera namn från kompanjon

  var life = empty(rows, cols)
  val cellSize = 25
  val window: PixelWindow = new PixelWindow(rows*cellSize, cols*cellSize, "Epic Life")
  var quit = false
  var play = false

  //def fromString(s: String, rowDelim: String="\n", alive: Char= '0') = {

  //}

  def drawGrid(): Unit = {
    for(v <- 0 until rows; h <- 0 until cols) {
      window.line(v*cellSize,0, v*cellSize, cols*cellSize, Darkred)
      window.line(0,h*cellSize, rows*cellSize, h*cellSize, Darkred)
    }
  }

  def drawCell(row: Int, col: Int): Unit = {
    window.fill((row*cellSize) + 1, (col*cellSize) + 1, cellSize - 2, cellSize - 2, MrGreen)
  }
  def killCell(row: Int, col: Int): Unit ={
    window.fill((row*cellSize) + 1, (col*cellSize) + 1, cellSize - 2, cellSize - 2, Black)
  }

  def testDraw: Unit ={
    window.fill(10*cellSize, 10*cellSize, cellSize, cellSize, MrGreen)
  }

  def update(newLife: Life): Unit = {
    val oldLife = life
    life = newLife
    life.cells.foreachIndex{
      (r, c) => if(oldLife.apply(r, c) != life.apply(r, c)){
        if(newLife.apply(r, c)) drawCell(r, c) else killCell(r, c)
      }
    }
  }

  def handleKey(key: String): Unit = {
    if (key.toLowerCase == "enter") {update(life.evolved()); play = false}
    else if (key == " ") play = !play
    else if (key.toLowerCase == "r") {
      for (r <- 0 until rows; c <- 0 until cols) {
        life.updated(r, c, false)
        killCell(r, c);
      }
        update(random((rows, cols))); play = false
    }
    else if (key.toLowerCase == "backspace") {for(r <- 0 until rows; c <- 0 until cols){
      life.updated(r, c, false); killCell(r, c)}; play = false}
    else if (key.toLowerCase == "s") { introprog.IO.saveString(life.toString, introprog.Dialog.file("Save Life"))}
   // else if (key.toLowerCase == "o") {introprog.IO.loadString()}
  }
  def handleClick(pos: (Int, Int)): Unit = update(life.toggled((pos._1/cellSize), (pos._2/cellSize)))

  def loopUntilQuit(): Unit = while (!quit) {
    val t0 = System.currentTimeMillis
    if (play) update(life.evolved())
    window.awaitEvent(EventMaxWait)
    while (window.lastEventType != PixelWindow.Event.Undefined) {
      window.lastEventType match {
        case Event.KeyPressed  =>  handleKey(window.lastKey)
        case Event.MousePressed => handleClick(window.lastMousePos)
        case Event.WindowClosed => quit = true
        case _ =>
      }
      window.awaitEvent(EventMaxWait)
    }
    val elapsed = System.currentTimeMillis - t0
    Thread.sleep((NextGenerationDelay - elapsed) max 0)
  }

  def start(): Unit = { drawGrid(); loopUntilQuit() }
}

package life

case class Life(cells: Matrix[Boolean]) {

  /** Ger true om cellen på plats (row, col) är vid liv annars false.
    * Ger false om indexeringen är utanför universums gränser.
    */
  def apply(row: Int, col: Int): Boolean = {
    if(row < 0 || col < 0 || row > cells.dim._1-1 || col > cells.dim._2-1) false
    else cells.apply(row, col)
  }

  /** Sätter status på cellen på plats (row, col) till value. */
  def updated(row: Int, col: Int, value: Boolean): Life =
    Life(cells.updated(row, col)(value))

  /** Växlar status på cellen på plats (row, col). */
  def toggled(row: Int, col: Int): Life = {
    if (cells.apply(row, col)){
      updated(row, col, false)
    }
    else updated(row, col, true)
  }

  /** Räknar antalet levande grannar till cellen i (row, col). */
  def nbrOfNeighbours(row: Int, col: Int): Int = {
    var n = 0
    for(r <- -1 to 1; c <- -1 to 1){
      if(r==0){
        if(c==0) n += 0
        else{
          if(apply(row+r, col+c))
            n += 1
          else n += 0
        }
      }
      else if(apply(row+r, col+c))
        n += 1
      else n += 0
    }
    n
  }

  /** Skapar en ny Life-instans med nästa generation av universum.
    * Detta sker genom att applicera funktionen \code{rule} på cellerna.
    */
  def evolved(rule: (Int, Int, Life) => Boolean = Life.defaultRule):Life = {
    var nextGeneration = Life.empty(cells.dim)
    cells.foreachIndex { (r,c) =>
    nextGeneration = nextGeneration.updated(r, c, rule(r, c, Life(cells)))
    }
    nextGeneration
  }

  /** Radseparerad text där 0 är levande cell och - är död cell. */
  override def toString = {
    var cellsString: String = ""
    for(r <- 0 until cells.dim._1) {
      for (c <- 0 until cells.dim._2) {
        if (cells.apply(r, c)) cellsString = cellsString + "0"
        else cellsString = cellsString + "-"
      }
      cellsString = cellsString + "\n"
    }
    cellsString
  }
}

object Life {
  /** Skapar ett universum med döda celler. */
  def empty(dim: (Int, Int)): Life = Life(Matrix.fill(dim)(false))

  /** Skapar ett unviversum med slumpmässigt liv. */
  def random(dim: (Int, Int)): Life = {
    val r = Vector.fill(dim._1, dim._2)(if((Math.random*4).toInt == 0) true else false)
    Life(Matrix(r))
  }

  /** Implementerar reglerna enligt Conways Game of Life. */
  def defaultRule(row: Int, col: Int, current: Life): Boolean = {
    if(current.apply(row, col)){
      if (Vector(2, 3) contains current.nbrOfNeighbours(row, col)) {
        true
      } else false
    } else if(current.nbrOfNeighbours(row, col) == 3) {
        true
      } else false
    }
}

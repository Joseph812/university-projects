package blockbattle

object Game {
  val windowSize = (30, 50)
  val windowTitle = "EPIC BLOCK BATTLE"
  val blockSize = 14
  val skyRange    = 0 to 7
  val grassRange  = 8 to 8
  import java.awt.{Color => JColor}
  object Color {
    val black = new JColor(0, 0, 0)
    val mole = new JColor(51, 51, 0)
    val mole2 = new JColor(151, 51, 0)
    val soil = new JColor(153, 102, 51)
    val tunnel = new JColor(204, 153, 102)
    val tunnel1 = new JColor(224, 137, 93)
    val tunnel2 = new JColor(255, 124, 78)
    val sky = new JColor(51,153,255)
    val grass = new JColor(0, 204, 0)
  }
  def backgroundColorAtDepth(y: Int): java.awt.Color = {
    if (y <= 8*blockSize) {Color.sky}
    else if(y == 9*blockSize) {Color.grass}
    else Color.soil
  }
}

class Game(
  val leftPlayerName: String  = "PLAYER ONE",
  val rightPlayerName: String = "PLAYER TWO"
  ) {
    import Game._
    val window    = new BlockWindow(windowSize, windowTitle, blockSize)
    val windowEdge = Pos(windowSize._1*blockSize, windowSize._2*blockSize)
    def RNG (): Int = (math.random*100).toInt
    def RNGPos (): Pos = {
     var slumpBnmP = Pos(RNG(), RNG())
     var fixedslumpBx = ((slumpBnmP.x - (blockSize % slumpBnmP.x))*blockSize)/100
     var fixedslumpBy = ((slumpBnmP.y - (blockSize % slumpBnmP.y))*blockSize)/100
     var slumpPos = (fixedslumpBx-blockSize, fixedslumpBy-blockSize)
     while (slumpPos._2 < (9*blockSize)) {
       var slumpPos = (fixedslumpBx-blockSize, fixedslumpBy-blockSize)
     }
     var posp = Pos(slumpPos._1, slumpPos._2)
     posp
    }

   //  var slumpBnm1x = RNG()
   //  var slumpBnm2x = (math.random*100).toInt
   //  var slumpBnm1y = (math.random*100).toInt
   //  var slumpBnm2y = (math.random*100).toInt
   //  var fixedslumpB1x = ((slumpBnm1x - (blockSize % slumpBnm1x))*blockSize)/100
   //  var fixedslumpB2x = ((slumpBnm2x - (blockSize % slumpBnm2x))*blockSize)/100
   //  var fixedslumpB1y = ((slumpBnm1y - (blockSize % slumpBnm1y))*blockSize)/100
   //  var fixedslumpB2y = ((slumpBnm2y - (blockSize % slumpBnm2y))*blockSize)/100
   //  var slumpPos1 = (fixedslumpB1x-blockSize, fixedslumpB1y-blockSize)
   //  var slumpPos2 = (fixedslumpB1x-blockSize, fixedslumpB1y-blockSize)
   //  while (slumpPos1._2 < (9*blockSize)) {
   //  var slumpPos1 = (fixedslumpB1x-blockSize, fixedslumpB1y-blockSize)
   // }
   // while(slumpPos2._2 < (9*blockSize)) {
   //  var slumpPos2 = (fixedslumpB1x-blockSize, fixedslumpB1y-blockSize)
   //  }
   //  val posp1 = Pos(slumpPos1._1, slumpPos1._2)
   //  val posp2 = Pos(slumpPos2._1, slumpPos2._2)
    val kc1 = KeyControl("a", "d", "w", "s")
    val kc2 = KeyControl("Left", "Right", "Up", "Down" )
    val startdir1 = kc1.direction("nothing")
    val startdir2 = kc2.direction("nothing")
    val leftMole = new Mole(leftPlayerName, RNGPos(), startdir1, Color.mole, Color.tunnel1, kc1)
    val rightMole = new Mole(rightPlayerName, RNGPos(), startdir2, Color.mole2, Color.tunnel2, kc2)
    var time = 60.0



    def drawWorld(): Unit = {
      import Color._
      // for (x <- 0 until windowEdge.x) {
      //   for (y <- 0 until windowEdge.y) {
      //     var xy = Pos(x, y)
      //     window.setBlock(xy, backgroundColorAtDepth(y))
      //   }
      // }
      val leftTopSoil = Pos(0, 9*blockSize)
      val leftTopSky = Pos(0,0)
      val leftTopGrass = Pos(0,8*blockSize)
      window.setBlockwSize(leftTopSky, windowSize._1, windowSize._2, sky)
      window.setBlockwSize(leftTopGrass, windowSize._1, windowSize._2, grass)
      window.setBlockwSize(leftTopSoil, windowSize._1, windowSize._2, soil)
      window.setBlock(leftMole.pos, leftMole.color)
      window.setBlock(rightMole.pos, rightMole.color)
    }

    // def startMolePos(mole: Mole, kc: KeyControl, moleColor: java.awt.Color): Pos = {
    //   var slumpBnmx = (math.random*100).toInt
    //   var slumpBnmy = (math.random*100).toInt
    //   var fixedslumpBx = ((slumpBnmx - (blockSize % slumpBnmx))*blockSize)/100
    //   var fixedslumpBy = ((slumpBnmy - (blockSize % slumpBnmy))*blockSize)/100
    //   var slumpPos = (fixedslumpBx-blockSize, fixedslumpBy-blockSize)
    //   while (slumpPos._2 < (9*blockSize)) {
    //   var slumpPos = (fixedslumpBx-blockSize, fixedslumpBy-blockSize)
    //   val startdir = kc1.direction("nothing")
    //   val mole = new Mole(mole.name, )
    //  }

    //}

    def eraseBlocks(x1: Int, y1: Int, x2: Int, y2: Int): Unit = {
      var leftTopeBlocks = Pos(x1, y1)
      window.setBlockwSize(leftTopeBlocks, x2, y2, Color.black)
    }

    def update(mole: Mole): Unit = {
      window.setBlock(mole.nextPos, mole.color)
      val illigalPos = Pos(mole.pos.x, 8*blockSize)
      if ( mole.pos.y > illigalPos.y ) {
      window.setBlock(mole.pos, mole.tunnel)
    }
    else {
      window.setBlock(mole.pos, Color.grass)
      println(s"${mole.name} is trying to dig in the grass! You lose points!")
      mole.points = mole.points - 30
  }
      mole.move()
    }

    def boundaryWall(mole: Mole): Unit = {
      var restSlumpPos = Pos(blockSize % mole.pos.x, blockSize % mole.pos.y)
      if (mole.nextPos.y < (8*blockSize - restSlumpPos.y)) {
        mole.setDir(mole.keyControl.down)
      }
      if (mole.nextPos.y > (windowEdge.y - blockSize + restSlumpPos.y)) {
        mole.setDir(mole.keyControl.up)
      }
      if (mole.nextPos.x < restSlumpPos.x) {
        mole.setDir(mole.keyControl.right)
      }
      if (mole.nextPos.x > windowEdge.x - restSlumpPos.x) {
        mole.setDir(mole.keyControl.left)
      }
    }

    def showPoints() = {
      var p1 = windowSize._1-blockSize
      val text1Pos = Pos(1, 1)
      val text2Pos = Pos(p1, 1)
      val text1PosB = Pos(8*blockSize, blockSize)
      val text2PosB = Pos((blockSize*p1)+(7*blockSize), blockSize)
      window.setBlockwSize(text1PosB, 3, 2, Color.sky)
      window.write(s"$leftPlayerName : ${leftMole.points}", text1Pos, Color.black)
      window.setBlockwSize(text2PosB, 3, 2, Color.sky)
      window.write(s"$rightPlayerName : ${rightMole.points}", text2Pos, Color.black)
    }

    def addPoints(mole: Mole): Unit = {
      if(mole.dir == (0, -1)) {
        var nextUpPos = Pos(mole.pos.x, mole.pos.y-1)
        var nextUpColor = window.getBlock(nextUpPos)
        if(nextUpColor != mole.tunnel) mole.points = mole.points + 1
        else mole.points = mole.points + 0
      }
      if(mole.dir == (-1, 0)) {
        var nextLeftPos = Pos(mole.pos.x-1, mole.pos.y)
        var nextLeftColor = window.getBlock(nextLeftPos)
        if(nextLeftColor != mole.tunnel) mole.points = mole.points + 1
        else mole.points = mole.points + 0
      }
      if(mole.dir == (1, 0)) {
        var nextRightPos = Pos(mole.pos.x+1, mole.pos.y)
        var nextRightColor = window.getBlock(nextRightPos)
        if(nextRightColor != mole.tunnel) mole.points = mole.points + 1
        else mole.points = mole.points + 0
      }
      if(mole.dir == (0, 1)) {
        var nextDownPos = Pos(mole.pos.x, mole.pos.y+1)
        var nextDownColor = window.getBlock(nextDownPos)
        if(nextDownColor != mole.tunnel) mole.points = mole.points + 1
        else mole.points = mole.points + 0
      }
      else mole.points = mole.points + 0
    }

    def timer(): Unit = {
      val midx = (windowSize._1/2 - blockSize/3)
      val midxB = (windowEdge.x/2 - blockSize/3)
      val timePos = Pos(midx, 3)
      val timePosB = Pos(midxB+blockSize/4, 3*blockSize)
      var timePosB2 = Pos(midxB+(3*blockSize/2), 3*blockSize)
      time = time - 0.085
      window.setBlockwSize(timePosB, 10, 3, Color.sky)
      window.write(s"Time : $time",timePos , Color.black)
      window.setBlockwSize(timePosB2, 12, 3, Color.sky)
    }

   def gameOver() = {
     if (time < 0) {
     val midgx = (windowSize._1/2 - blockSize/3)
     val ggPos = Pos(midgx, windowSize._2/2)
     window.write("Game Over", ggPos, Color.black)
     quit = true
     }
   }

   def winner(mole: Mole, mole2: Mole){
     val midwx = (windowSize._1/2 - blockSize)
     val winnerPos = Pos(midwx, windowSize._2/2+1)
     if(quit){
       if(mole.points > mole2.points) {
       window.write(s"The winner is : ${mole.name} with ${mole.points} points!", winnerPos, Color.black)
       }
       if(mole.points < mole2.points) {
       window.write(s"The winner is : ${mole2.name} with ${mole2.points} points!", winnerPos, Color.black)
       }
       if(mole.points == mole2.points) window.write("It's a tie!", winnerPos, Color.black)
     }
   }

   def handleEvents(): Unit = {
      var e = window.nextEvent()
      while (e != BlockWindow.Event.Undefined) {
          e match {
            case BlockWindow.Event.KeyPressed(key) => {
              println(s"Keypressed: $key")
              leftMole.setDir(key)
              rightMole.setDir(key)
              leftMole.nextPos
              rightMole.nextPos
              // update(leftMole)
              // update(rightMole)
            }
            case BlockWindow.Event.WindowClosed => {
              quit = true
              println("Thanks for playing!")
            }
          }
          e = window.nextEvent()
      }
    }

    var quit = false
    val delayMillis = 80

    def gameLoop(): Unit = {
      while(!quit) {
        val t0 = System.currentTimeMillis
        addPoints(leftMole)
        addPoints(rightMole)
        showPoints()
        handleEvents()
        update(leftMole)
        update(rightMole)
        boundaryWall(leftMole)
        boundaryWall(rightMole)
        timer()
        gameOver()
        winner(leftMole, rightMole)
        val elapsedMillis = (System.currentTimeMillis - t0).toInt
        Thread.sleep((delayMillis - elapsedMillis) max 0)
      }
    }

    def start(): Unit = {
      println("Start digging!")
      println(s"$leftPlayerName ${leftMole.keyControl}")
      println(s"$rightPlayerName ${rightMole.keyControl}")
      drawWorld()
      gameLoop()
    }
}

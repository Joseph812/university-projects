package blockbattle

case class KeyControl (left: String, right: String, up: String, down: String) {
  def direction(key: String): (Int, Int) = {
    if (key == up) (0, 0-1)
    else if (key == left) (0-1, 0)
    else if (key == right) (0+1, 0)
    else if (key == down) (0, 0+1)
    else (0, 0)
  }

    def has(key: String): Boolean = {
    if (key == up) true
    else if (key == left) true
    else if (key == right) true
    else if (key == down) true
    else false
    }
  }

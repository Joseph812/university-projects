package blockbattle

case class Pos(x: Int, y: Int) {
  def moved(delta: (Int, Int)): Pos = {
    val b = (delta._1*Game.blockSize, delta._2*Game.blockSize )
    Pos(x + b._1, y + b._2)
  }
}

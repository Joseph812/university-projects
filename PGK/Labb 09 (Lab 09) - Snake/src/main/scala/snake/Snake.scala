package snake

class Snake (
              val initPos: Pos,
              val initDir: Dir,
              val headColor: java.awt.Color,
              val tailColor: java.awt.Color,
              val game: SnakeGame
            ) extends CanMove {
  var dir: Dir = initDir

  val initBody: List[Pos] = List(initPos + initDir, initPos)

  val body: scala.collection.mutable.Buffer[Pos] = initBody.toBuffer

  val initTailSize: Int = 10 // välj själv vad som är lagom svårt
  var tailSize: Int = initTailSize

  var nbrOfMoves = 0
  var nbrOfStepsSinceReset = 0
  val growEvery = 50
  val startGrowingAfter = 100
  var nbrOfApples = 0
  var nbrOfBananas = 0
  val pOfHole = 0.10

  def reset(): Unit = { // återställ starttillstånd, ge rätt svanslängd
    dir = initDir
    body.clear()
    initPos +=: body
    (initPos + initDir) +=: body
    nbrOfStepsSinceReset = 0
    tailSize = 10
    nbrOfMoves = 0
    movesPerSecond = 20
    nbrOfBananas = 0
    nbrOfApples = 0
  }

  def grow(): Unit = body.head.+(dir) +=: body // väx i rätt riktning med extra svansposition

  def shrink(): Unit = {
    if (tailSize < body.length || body.length <=2) { // krymp svansen om kroppslängden är större än 2
      body.remove(body.length-1)
    }
  }

  def dropBody(): Unit = body.dropWhile(_ => body.size > 2)

  def isOccupyingBlockAt(p: Pos): Boolean = body.contains(p)    // kolla om p finns i kroppen

  def isHeadCollision(other: Snake): Boolean = body.head == other.body.head // kolla om huvudena krockar
  def isTailCollision(other: Snake): Boolean = other.body.tail.contains(body.head) // mitt huvud i annans svans

  def move(): Unit = {   // väx och krymp enl. regler; action om äter frukt
    grow()

    nbrOfStepsSinceReset += 1
    nbrOfMoves +=1

    //If 2 player game
    if (game.players.size== 2){
      if (pOfHole > scala.util.Random.nextDouble() && body.length >= 5) {
        game.eraseBlock(body(2).x, body(2).y)
        body.remove(2)
        movesPerSecond = 10
      }
    }else{//If one player game
      if (nbrOfStepsSinceReset >= startGrowingAfter && nbrOfStepsSinceReset % growEvery == 0){
        tailSize += 1
        movesPerSecond += 1 // Increase speed every few moves
      }
      shrink()
    }

    game.entities.foreach(x => {
      if (x.isInstanceOf[Apple] && x.isOccupyingBlockAt(body.head)){
        tailSize += 1
        nbrOfApples += 1 //For score
        x.reset()
      }
      else if(x.isInstanceOf[Banana] && x.isOccupyingBlockAt(body.head)){
      //  nbrOfApples += 3
        nbrOfBananas += 1

        x.reset()
        //game.entities.drop(game.entities.length-1)
      }
    })
  }

  def draw(): Unit = {
    body.foreach(pos => game.drawBlock(pos.x, pos.y, tailColor))
    game.drawBlock(body.head.x, body.head.y, headColor)
  }

  def erase(): Unit = game.eraseBlock(body.last.x, body.last.y)

  override def toString: String =  // bra vid println-debugging
    body.map(p => (p.x, p.y)).mkString(">:)", "~", s" going $dir")
}


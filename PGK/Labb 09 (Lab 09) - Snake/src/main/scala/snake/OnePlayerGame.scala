package snake

class OnePlayerGame extends SnakeGame("Solo Snake") { // ska ärva SnakeGame

  val Snake1 = new Snake(initPos = Pos(5, 6, Dim(dim)), initDir = West, Colors.DarkGreen, Colors.Green, this)
  def appleMaker(n: Int): Unit =  {
    for(i <- 0 until n){
      entities = entities :+ new Apple(Colors.Apple, this)
    }
  }
  def bananaMaker(n: Int): Unit = {
    for(i <- 0 until n){
      entities = entities :+ new Banana(Colors.Banana, this)
    }
  }
  // orm, äpple, ev. bananer etc
  override def isGameOver(): Boolean = {
    Snake1.isTailCollision(Snake1) || Snake1.nbrOfApples == 3
  }
  var points = 0
  override def updateScore() = {
  //  val p = points
    if (Snake1.isTailCollision(Snake1)) {

      points = (Snake1.nbrOfBananas*100) - (Snake1.nbrOfMoves + Snake1.nbrOfApples*200)
    } else {
      points = (Snake1.nbrOfBananas*100) - (Snake1.nbrOfMoves/2 + Snake1.nbrOfApples*200)
    }
    clearMessageArea()
    drawTextInMessageArea(s"Points: $points", 1, 1, Colors.Green)
    if(points % 1000 == 1) appleMaker(5)

  }

  def scoreWinner(): Unit = {
    clearMessageArea()
    drawTextInMessageArea(s"Congratulations!! You got $points points!                    Press space to restart.", 1, 1, Colors.Green)
    points = 0
    Snake1.nbrOfApples = 0
    Snake1.nbrOfBananas = 0
  }

  def play(playerNames: String*): Unit = {
    val P1 = Player(playerNames(0), "A", "S", "D", "W", Snake1)
    println(P1)
    players = players :+ P1
    entities = entities :+ Snake1
    appleMaker(5)
    bananaMaker(1)
    startGameLoop()
  }
}

package snake

class Apple(color: java.awt.Color, game2: SnakeGame) extends CanTeleport {
  def teleportAfterSteps: Int = 200

  def game: SnakeGame = game2

  def draw():  Unit = {
    while(game2.players.exists(_.snake.isOccupyingBlockAt(pos))){
      reset()
    }
    game.drawBlock(pos.x, pos.y, color)
  }

  def erase():  Unit = game.eraseBlock(pos.x, pos.y)

  def isOccupyingBlockAt(p: Pos): Boolean = pos == p
}



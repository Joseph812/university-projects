package snake

class Banana(color: java.awt.Color, game2: SnakeGame) extends CanTeleport {
  def teleportAfterSteps: Int = 150

  def game: SnakeGame = game2

  def draw(): Unit = {
    while(pos.y > game2.dim._2 - 3 || game2.players.exists(_.snake.isOccupyingBlockAt(pos))){
      reset()
    }
    for (i <- 0 to 2) game.drawBlock(pos.x, pos.y + i, color)
  }

  def erase(): Unit = {
    for (i <- 0 to 2) game.eraseBlock(pos.x, pos.y + i)
  }

  def isOccupyingBlockAt(p: Pos): Boolean = {
    pos == p || Pos(pos.x, pos.y + 1, Dim(game2.dim)) == p || Pos(pos.x, pos.y + 2, Dim(game2.dim)) == p
  }
}

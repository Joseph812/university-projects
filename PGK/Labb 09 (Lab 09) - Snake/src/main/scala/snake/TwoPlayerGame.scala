
package snake



class TwoPlayerGame extends SnakeGame("Ready Player Two"){  // ska ärva SnakeGame

  // ormar och ev. äpple, bananer etc

  var scoreWinnah = ""

  val readyPlayerOne: Snake = new Snake(initPos = Pos(dim._1/2,0, Dim(dim)),initDir = West,headColor = Colors.DarkGreen ,tailColor = Colors.Green, game = this  )

  val readyPlayerTwo: Snake = new Snake(initPos = Pos(dim._1/2,dim._2-1, Dim(dim)),initDir = East,headColor = Colors.DarkBlue,tailColor = Colors.Blue,game = this)



  override def updateScore(): Unit = {

    clearMessageArea()

    drawTextInMessageArea(s"Poängen för Player one är ${readyPlayerOne.nbrOfApples}", 1,1, Colors.Green,blockSize)

    drawTextInMessageArea(s"Poängen för Player two är ${readyPlayerTwo.nbrOfApples}", dim._1/2,1, Colors.Blue,blockSize)

    //if (readyPlayerOne.nbrOfBananas) {readyPlayerOne.nbrOfApples+=3 }

    //else if (readyPlayerTwo.nbrOfBananas == 1) {readyPlayerTwo.nbrOfApples+=3 }

    //if (readyPlayerTwo.nbrOfApples + readyPlayerOne.nbrOfApples == 3) {entities = entities :+ new Banana(Colors.Banana,this); readyPlayerOne.nbrOfApples += 1; readyPlayerTwo.nbrOfApples +=1 }


  }



  override def isGameOver(): Boolean = {

    if (readyPlayerOne.isHeadCollision(readyPlayerTwo)) {scoreWinnah = ("is a tie"); true}

    else if (readyPlayerTwo.isHeadCollision(readyPlayerOne)){scoreWinnah = "is a tie";true}

    else if (readyPlayerOne.isTailCollision(readyPlayerTwo) ) {scoreWinnah = ("The winner is player 2"); true}

    else if (readyPlayerTwo.isTailCollision(readyPlayerOne)) {scoreWinnah = ("The winner is player 1"); true}

    else if (readyPlayerOne.isTailCollision(readyPlayerOne)) {scoreWinnah = ("Player 1 self-crashed"); true}

    else if (readyPlayerTwo.isTailCollision(readyPlayerTwo)) {scoreWinnah = ("Player 2 self-crashed"); true}

    else false




  }


  override def scoreWinner(): Unit = {

    if ((scoreWinnah == "is a tie") && readyPlayerOne.nbrOfApples < readyPlayerTwo.nbrOfApples) scoreWinnah = "The winner is player 2"

    else if ((scoreWinnah == "is a tie") && readyPlayerOne.nbrOfApples > readyPlayerTwo.nbrOfApples) scoreWinnah = "The winner is player 1"

    drawTextInMessageArea(s"$scoreWinnah", dim._1/2,1)

    readyPlayerOne.nbrOfApples = 0

    readyPlayerTwo.nbrOfApples = 0



  }




  def play(playerNames: String*): Unit = {


    val player1: Player = new Player(playerNames(0), "A","S","D","W",readyPlayerOne)

    val player2: Player = new Player(playerNames(1), "J", "K", "L", "I", readyPlayerTwo)

    def bananaMaker(nbr: Int): Unit = {


      for (i <- 0 until nbr){

        entities = entities :+ new Banana(Colors.Banana,this)

      }

    }

    def appleMaker(iNbr: Int): Unit = {


      for (i <- 0 until iNbr){

        entities = entities :+ new Apple(Colors.Apple,this)

      }

    }



    players = players :+ player1

    entities = entities :+ readyPlayerOne

    players = players :+ player2

    entities = entities :+ readyPlayerTwo

    bananaMaker(1)

    appleMaker(3)

    startGameLoop()






  }  // ska överskugga play i SnakeGame

}


object wordkill2{
  var x = -1
  var nivå = 0
  var poäng = 0
  def start(args: Array[String]): Unit = {
    println("""
            |----------------------------------------------------------------
            |----------------------------------------------------------------
            |-----------------Välkommen till WordKill!!!!--------------------
            |----------------------------------------------------------------
            |----Allt du behöver göra är att döda mina ord så fort du kan----
            |----------------------------------------------------------------
            |----------------Stavar du fel så forlorar du.-------------------
            |----------------------------------------------------------------
            |----------------------------------------------------------------
            |""".stripMargin)
  }
    def redostart(args: Array[String]): Unit = {
    if (x== -1) {
      var redo = scala.io.StdIn.readLine("Är du redo?"+"(Ja/Nej)")
      if (redo.toString == "Ja": Boolean) {
        x=x+1
        println("""----------------------------------------------------------------
                |----------------------------------------------------------------""".stripMargin)
      }
      else {
        println("Hejdå")
      }
    }
  }
  def vapenoption(args: Array[String]): Unit = {
      if (x== 0) {
        var vapen = scala.io.StdIn.readLine("""Välj din vapen:
                                            |
                                            |1. AK-47
                                            |2. Tangentbord
                                            |3. Äpple
                                            |
                                            |""".stripMargin)
        if (vapen.toString == "2": Boolean) {
          x=x+1
          println("""----------------------------------------------------------------
                  |-------------------------Du valde rätt--------------------------
                  |----------------------------------------------------------------""".stripMargin)
        }
        else {
          if (vapen.toString == "1": Boolean) {
            x=x-10
            println("""----------------------------------------------------------------
                  |----------------Du kan inte döda ord med AK-47------------------
                  |----------------------------------------------------------------""".stripMargin)
          }
            if (vapen.toString == "3": Boolean) {
              x=x-10
              println("""----------------------------------------------------------------
                      |-------------Det verkar som att du är hungrig--------------------
                      |----------------------------------------------------------------""".stripMargin)
          }
            else {
              x=x-10
              println("Nej")
            }
        }
      }
    }
    def spelet(args: Array[String]): Unit = {
    if (x==1){
      var svar = scala.io.StdIn.readLine("Nivå 1"+ ". "+"Nolla"+" :")
      try{
        if (svar.toString == "Nolla": Boolean) {
            var q = (math.random*15).toInt
          poäng=poäng+q
          x=x+1
          nivå=nivå+1
          println(s"""
                  |----------------------------------------------------------------
                  |--------------Grattis, du har fått $poäng poäng :D-------------------
                  |----------------------------------------------------------------
                  |""".stripMargin);
                }
        else {
        x=x-10
        println("Fel");
      }
    }
    catch { case e: Exception => println("Oj något har gått fel :C")}
    }
    if (x==2) {
      var svar = scala.io.StdIn.readLine("Nivå 2"+ ". "+"Det är fint väder idag"+" :")
      try{
        if (svar.toString == "Det är fint väder idag": Boolean) {
          x=x+1
          nivå=nivå+1
          var q = (math.random*100).toInt
          poäng=poäng+q
          println(s"""
                  |----------------------------------------------------------------
                  |---------------Grattis, du har fått $q poäng :D-----------------
                  |----------------------------------------------------------------
                  |""".stripMargin);
          }
        else {
          x=x-10
          println("Fel");
        }
      }
      catch { case e: Exception => println("Oj något har gått fel :C")}
  }
  if (x==3) {
    var svar = scala.io.StdIn.readLine("Nivå 3"+ ". "+"491241904191481094190"+" :")
    try{
      if (svar.toString == "491241904191481094190": Boolean) {
        var q=(math.random*200).toInt
        poäng=poäng+q
        nivå=nivå+1
        x=x+1
        println(s"""
                |----------------------------------------------------------------
                |---------------Grattis, du har fått $q poäng :D----------------
                |----------------------------------------------------------------
                |""".stripMargin);
        }
      else {
        x=x-10
        println("Fel");
      }
    }
    catch { case e: Exception => println("Oj något har gått fel :C")}
  }
  if (x==4) {
    var svar = scala.io.StdIn.readLine("Nivå 4"+ ". "+"BU677Ibjk8nkuht7787GjIGVBhjkn"+" :")
    try{
      if (svar.toString == "BU677Ibjk8nkuht7787GjIGVBhjkn": Boolean) {
        var q=(math.random*300).toInt
        poäng=poäng+q
        nivå=nivå+1
        x=x+1
        println(s"""
                |----------------------------------------------------------------
                |---------------Grattis, du har fått $q poäng :D----------------
                |----------------------------------------------------------------
                |""".stripMargin);
        }
      else {
        x=x-10
        println("Fel");
      }
    }
    catch { case e: Exception => println("Oj något har gått fel :C")}
  }
  if (x==5) {
    var svar = scala.io.StdIn.readLine("Nivå 5"+ ". "+"100001 101110 110101"+" :")
    try{
      if (svar.toString == "33 46 53": Boolean) {
        var q=(math.random*1000).toInt
        poäng=poäng+q
        x=x+1
        nivå=nivå+1
        println(s"""
                |----------------------------------------------------------------
                |---------------Grattis, du har fått $q poäng :D----------------
                |----------------------------------------------------------------
                |""".stripMargin);
        }
      else {
        x=x-10
        println("Fel");
      }
    }
    catch { case e: Exception => println("Oj något har gått fel :C")}
    }
  }
  var highscore = 1131
  def end(args: Array[String]): Unit = {
  if (poäng>highscore){
    highscore = poäng
    println(s"""
            |************************************************************
            |*----------------------------------------------------------*
            |*------------------★★★-CONGRATULATIONS-★★★-----------------*
            |*----------------------------------------------------------*
            |*----------★★★-DU HAR FÅTT EN NY HIGHSCORE AV-★★★----------*
            |*-----------------------<$highscore>-----------------------------*
            |*----------------------------------------------------------*
            |************************************************************
            |""".stripMargin)
          }

          if(nivå==5){
            println(s"""----------------------------------------------------------------
                      |----------------Grattis du har klarat alla nivåer!---------------
                      |----------------------------------------------------------------
                      |--------------------Du har nått nivå $nivå av 5---------------------
                      |------------------------Du har $poäng poäng-----------------------
                      |----------------------------------------------------------------
                      |------------------------Highescore: $highscore -----------------------
                      |----------------------------------------------------------------""".stripMargin)
          }
          else {
            println(s"""----------------------------------------------------------------
                      |---------------------------GameOver-----------------------------
                      |----------------------------------------------------------------
                      |--------------------Du har nått nivå $nivå av 5---------------------
                      |------------------------Du har $poäng poäng-----------------------
                      |----------------------------------------------------------------
                      |------------------------Highescore: $highscore -----------------------
                      |----------------------------------------------------------------""".stripMargin)
           }
        }
    }
}
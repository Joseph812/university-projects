package nlp

case class Text(source: String){
  lazy val words: Vector[String] = {
  val whiteSpaceFree = source.map(w => if(w.isWhitespace) ' ' else w)
  val onlyLetters = whiteSpaceFree.map(l => if(!l.isLetter) ' ' else l)
  val lowercaseLetters = onlyLetters.toLowerCase
  val splittedSource = lowercaseLetters.split(' ')
  val filteredSS = splittedSource filter (_ != " " )
  val filteredSS2 = filteredSS filter(_ != "")
  filteredSS2.toVector
  }  // dela upp source i ord

  lazy val distinct: Vector[String] = words.distinct

  lazy val wordSet: Set[String] = words.toSet

  lazy val wordsOfLength: Map[Int, Set[String]] = wordSet.groupBy(_.length)

  lazy val wordFreq: Map[String, Int] = {
  val wordsString = FreqMapBuilder()
  (0 until words.size).foreach(i => wordsString.add(words(i)))
  wordsString.toMap
  }

  def ngrams(n: Int): Vector[Vector[String]] = ???  // använd sliding

  lazy val bigrams: Vector[(String, String)] =
    ngrams(2).map(xs => (xs(0), xs(1)))

  lazy val followFreq: Map[String, Map[String, Int]] = ??? //nästlad tabell

  lazy val follows: Map[String, String] =
    followFreq.mapValues(_.maxBy(_._2)._1)
}

object Text {
  def fromFile(fileName: String, encoding: String = "UTF-8"): Text = {
    val source = scala.io.Source.fromFile(fileName, encoding).mkString
    new Text(source)
  }
  def fromURL(url: String, encoding: String = "UTF-8"): Text = {
    val source = scala.io.Source.fromURL(url, encoding).mkString
    new Text(source)
  }
}

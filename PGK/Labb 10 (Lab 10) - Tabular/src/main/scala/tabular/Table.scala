package tabular
import scala.util.Try

case class Table(
  headings: Vector[String],
  rows: Vector[Vector[Cell]],
  separator: Char
){
  val nRows = rows.length
  val nCols = rows.map(_.length).max
  val dim = (nRows, nCols)

  /** Uppgift: Ändra så att resultatet blir -1 om nyckel inte finns. */
  val indexOfHeading: Map[String, Int] = headings.zipWithIndex.toMap.withDefaultValue(-1)

  /** Cell på plats (row, col), Cell.empty om indexering utanför gräns. */
  def apply(row: Int, col: Int): Cell = Try(rows(row)(col)).getOrElse(Cell.empty)

  /** Some(cell) på plats (row, col), None om indexering utanför gräns. */
  def get(row: Int, col: Int): Option[Cell] = {
    if(apply(row, col) == Cell.empty) None else Some(rows(row)(col))
  } //

  def row(index: Int): Vector[Cell] = rows(index)

  def col(index: Int): Vector[Cell] = rows.indices.map(apply(_, index)).toVector

  def col(heading: String): Vector[Cell] = col(indexOfHeading(heading))

  /** Registrering av frekvens för varje unikt värde i kolumn heading.*/
  def freq(heading: String): Vector[(Cell, Int)] = {
    val headingValues: Vector[Cell] = rows.map(x => x(headings indexOf heading))
    val headingValuesString: Vector[String] = headingValues.map(x => x.value)
    for (x <- headingValues.distinct) yield (x, headingValuesString.count(_ == x.value))
  }

  def filter(heading: String, values: Set[String]): Table =
    copy(rows = rows.filter(r => values.contains(r(indexOfHeading(heading)).value)))

  def sortOn(heading: String): Table =
    copy(rows = rows.sortBy(r => r(indexOfHeading(heading)).value))

  def values(heading: String): Vector[String] = col(heading).map(_.value)

  def show: String = {
    def maxValueLength(heading: String): Int = col(heading).map(_.value.length).max
    val colSize = headings.map(h => maxValueLength(h) max h.length)
    val paddedHeadings = headings.map(h => h.padTo(colSize(indexOfHeading(h)), ' '))
    val heading = paddedHeadings.mkString("|","|","|")
    val line = "-" * heading.length
    val paddedRows = rows.map(r =>
      headings.indices.map(i => r(i).value.padTo(colSize(i),' ')).mkString("|","|","|")
    ).mkString("\n")
    s"$line\n$heading\n$line\n$paddedRows\n$line\n (nRows, nCols) == $dim\n"
  }

  /** Strängvektor för varje rad i `rows`, kolumner skilda med `separator`. */
  def toLines(separator: Char): Vector[String] = {
    rows.map(x => x.map(y => y.value).mkString(s"$separator"))
  }
}

object Table {
  /** Skapa tabell från fil el. webbadress; kolumner skilda med `separator`. */
  def load(fileOrUrl: String, separator: Char): Table = {
    val source = fileOrUrl match {
      case url if url.startsWith("http")/*använd gard och startsWith*/ => scala.io.Source.fromURL(url)
      case path => scala.io.Source.fromFile(path)
    }
    val lines = try source.getLines.toVector finally source.close
    val rows = lines.map(x => x.split(separator).toVector) // kör split(separator).toVector på alla rader i lines
    Table(rows.head, rows.tail.map(_.map(Cell.apply)), separator)
  }
}

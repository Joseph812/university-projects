package tabular

import scala.util.Try
import introprog.{IO, Dialog}

object Command {
  var currentSeparator: Char = ','

  var currentTable: Option[Table] = None

  var originalTable: Option[Table] = None

  var fixedTable: Option[Table] = None

  val shortHelpText = "Type 'help' for help on commands and 'quit' to exit app."
  val longHelpText = """Commands:
    |help                 Print this list of commands
    |ls                   List files in current directory
    |load                 Load a file using introprog.Dialog.file
    |load filename|url    Load a table from filename or url using current separator
    |save                 Save a file using introprog.Dialog.file
    |save filename        Save current table to <filepath>
    |sep                  Show current column separator
    |sep c                Change current column separator to char c
    |show                 Print current table to the console
    |show orig            Print original table to the console
    |sort h               Sort current table on heading h
    |filter h a b c ...   Keep rows where heading h has values a b c ...
    |pie h                Draw a pie chart of current table's heading h
    |bar h                Draw a bar chart of current table's heading h
    |barOrig h            Draw a bar chart of original table's heading h
    |quit, Ctrl+D         Terminate app
    |""".stripMargin

   def fromUser(): Vector[String] = {
     // readLine can give null if Ctrl+D is typed by user (Linux, MacOS)
     val cmdOpt = Option(scala.io.StdIn.readLine("> ")) // None if null
     cmdOpt.map(_.split(' ').map(_.trim).toVector).getOrElse(Vector("quit"))
   }

   def TODO = "TODO: Not yet implemented."

   def load(fileOrUrl: String): String = {
     val tab: Table = Table.load(fileOrUrl, currentSeparator)
     val fixedHeadings: Vector[String] = tab.headings.map(s => s.replaceAllLiterally(" ", "_") )
     currentTable = Option(tab)
     originalTable = currentTable
     val fTable = Option(Table(fixedHeadings,tab.rows, tab.separator))
     "Table has been loaded."
   }

  def filter(h: String, s: Set[String]): String = {
    val toBeOrNotToBe: String = currentTable match {
      case Some(t) => currentTable = Option(t.filter(h, s));"Filtered."
      case _        => "Something is wrong with the table."
    }
    toBeOrNotToBe
  }

  def sort(head: String): String = {
    val toBeOrNotToBe: String = currentTable match {
      case Some(t) => currentTable = Option(t.sortOn(head)); "Table has been sorted."
      case _        => "Something is wrong with the table."
    }
    toBeOrNotToBe
  }

   def show(t: Option[Table]): String = {
     val testT = t.get
     val toBeOrNotToBe: String = testT match {
       case t: Table => t.show
       case _        => "Something is wrong with the table."
     }
     toBeOrNotToBe
   }

   def drawBar(t: Option[Table], h: String): String = {
     val test: Table = t.get
     val toBeOrNotToBe: String = test match {
       case t: Table => Graph.bar(t.freq(h).map(x => (x._1.value, x._2)), s"$h Bar Chart"); "Graph made."
       case _        => "Something is wrong with the table."
     }
     toBeOrNotToBe
   }

   def save(file: String): String = TODO

   def listFiles(): String = IO.currentDir +"\n"+ IO.list(IO.currentDir).mkString(" ")

   def doCommand(cmd: Vector[String]): String  = cmd match {
     case Vector("") | Vector()            => shortHelpText

     case Vector("help")                   => longHelpText

     case Vector("ls")                     => listFiles()

     case Vector("sep")                    => "Current separator is " + currentSeparator.toString + "."

     case Vector("sep", a)                 => currentSeparator = a(0); s"Current separator is changed to ${a.charAt(0)}."

     case Vector("sort", h)                => sort(h)

     case "filter" +: h                    => filter(h.head, h.tail.toSet)

     case Vector("bar", h)                 => drawBar(currentTable, h)

     case Vector("barOrig", h)             => drawBar(originalTable, h)

     case Vector("load", fileOrUrl)        => load(fileOrUrl)

     case Vector("show")                   => show(currentTable)

     case Vector("show orig")              => show(originalTable)

     case Vector("freqTest", h)            => currentTable.get.freq(h).toString

     case Vector("quit")                   => "quit"

     case _ => s"""Invalid command: ${cmd.mkString(" ")} \n$shortHelpText\n"""
   }

   def loopUntilQuit(): Unit = {
     println(shortHelpText)
     var quit = false
     while (!quit) {
       val result = doCommand(fromUser())
       if (result == "quit") quit = true else println(result)
     }
     println("Goodbye!")
   }
}

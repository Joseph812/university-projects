package blockmole

import java.awt.{Color => JColor}

object Color {
  val black = new JColor(0, 0, 0)
  val mole = new JColor(51, 51, 0)
  val soil = new JColor(153, 102, 51)
  val tunnel = new JColor(204, 153, 102)
  val sky = new JColor(51,153,255)
  val grass = new JColor(0, 204, 0)
}

object BlockWindow {
  import introprog.PixelWindow
  val windowSize = (30, 50)
  val blockSize = 10

  val window = new PixelWindow(windowSize._1 * blockSize, windowSize._2 * blockSize, "Blockmullvad")

  val b = blockSize
  type Pos = (Int, Int)
  def block(pos: Pos)(color: JColor = JColor.gray): Unit = {
    val x = pos._1*blockSize
    val y = pos._2*blockSize

    window.fill(x, y, b, b, color)
  }

  def rectangle(leftTop: Pos)(size: (Int, Int))(color: JColor = JColor.gray): Unit = {
    for (y <- leftTop._2 to size._2) {
      for (x <- leftTop._1 to size._1) {
        block(x,y) (color)
      }
    }
  }

  val maxWaitMillis = 10

  def waitForKey(): String = {
    window.awaitEvent(maxWaitMillis)
    while (window.lastEventType != PixelWindow.Event.KeyPressed) {
      window.awaitEvent(maxWaitMillis)
    }
    println(s"KeyPressed: " + window.lastKey)
    window.lastKey
  }

  def prevKey(): Unit = {
    val prevKey = window.lastKey
  }
}

object Mole {
  def delay(millis: Int): Unit = Thread.sleep(millis)
  def dig(): Unit = {
    var x = BlockWindow.windowSize._1 / 2
    var y = BlockWindow.windowSize._2 / 2
    var d = BlockWindow.b / 10
    var quit = false
    val prevKey = BlockWindow.prevKey
    while (!quit) {
      if (x < 0) {
        x = x+d
        BlockWindow.block(x, y)(Color.mole)
      }
      else if (x>29) {
        x = x-d
        BlockWindow.block(x, y)(Color.mole)
      }
      else if (y<10) {
        y = y+d
        BlockWindow.block(x, y)(Color.mole)
        BlockWindow.block(x, y-d)(Color.sky)
      }
      else if (y>49) {
        y = y-d
        BlockWindow.block(x, y)(Color.mole)
      }
      BlockWindow.block(x, y)(Color.mole)
      val key = BlockWindow.waitForKey()
      if      (key == "w") {
        while (prevKey == "w") {
          y=y-d
          BlockWindow.block(x, y)(Color.mole)
          BlockWindow.block(x, y+d)(Color.tunnel)
          delay(200)
        }
      }
      else if (key == "a") {
        while (prevKey == "a") {
          x=x-d
          BlockWindow.block(x, y)(Color.mole)
          if (y == 10){
            BlockWindow.block(x+d, y)(Color.grass)
          }
          else {
            BlockWindow.block(x+d, y)(Color.tunnel)
          }
          delay(200)
        }
      }
      else if (key == "s") {
        while (prevKey == "s") {
          y=y+d
          BlockWindow.block(x, y)(Color.mole)
          if (y == 11){
            BlockWindow.block(x, y-d)(Color.grass)
          }
          else {
            BlockWindow.block(x, y-d)(Color.tunnel)
          }
          delay(200)
        }
      }
      else if (key == "d") {
        while (prevKey == "d") {
          x= x+d
          BlockWindow.block(x, y)(Color.mole)
          if (y == 10){
            BlockWindow.block(x-d, y)(Color.grass)
          }
          else {
            BlockWindow.block(x-d, y)(Color.tunnel)
          }
          delay(200)
        }
      }
      else if (key == "q") quit = true
    }
  }
}

object Main {
  def drawWorld(): Unit = {
    import Color._
    BlockWindow.rectangle(0, 11)(30, 50) (soil)
    BlockWindow.rectangle(0, 0)(30, 9) (sky)
    BlockWindow.rectangle(0, 10)(30, 10) (grass)
  }

  def main(args: Array[String]): Unit = {
    drawWorld()
    Mole.dig()
  }
}

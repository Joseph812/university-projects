package textproc;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class Holgersson {

	public static final String[] REGIONS = { "blekinge", "bohuslän", "dalarna", "dalsland", "gotland", "gästrikland",
			"halland", "hälsingland", "härjedalen", "jämtland", "lappland", "medelpad", "närke", "skåne", "småland",
			"södermanland", "uppland", "värmland", "västerbotten", "västergötland", "västmanland", "ångermanland",
			"öland", "östergötland" };

	public static void main(String[] args) throws FileNotFoundException {

		long t0 = System.nanoTime();

		ArrayList<TextProcessor> list = new ArrayList<>();
		list.add(new SingleWordCounter("nils"));
		list.add(new SingleWordCounter("norge"));



		for (TextProcessor p: list) {

			Scanner s = new Scanner(new File("nilsholg.txt"));
			s.findWithinHorizon("\uFEFF", 1);
			s.useDelimiter("(\\s|,|\\.|:|;|!|\\?|'|\\\")+"); // se handledning

			while (s.hasNext()) {
				String word = s.next().toLowerCase();

				p.process(word);
			}

			s.close();

			p.report();
		}

		System.out.println(" ");

		MultiWordCounter q = new MultiWordCounter(REGIONS);
		Scanner z = new Scanner(new File("nilsholg.txt"));
		z.findWithinHorizon("\uFEFF", 1);
		z.useDelimiter("(\\s|,|\\.|:|;|!|\\?|'|\\\")+"); // se handledning

		while (z.hasNext()) {
			String word = z.next().toLowerCase();

			q.process(word);
		}

		z.close();

		q.report();

		System.out.println(" ");


		Scanner r = new Scanner(new File("nilsholg.txt"));
		Scanner r2 = new Scanner(new File("undantagsord.txt"));
		Set<String> stopwords = new HashSet<>();

		while(r2.hasNext()){
			stopwords.add(r2.next().toLowerCase());
		}
		r2.close();

		GeneralWordCounter g = new GeneralWordCounter(stopwords);
		r.findWithinHorizon("\uFEFF", 1);
		r.useDelimiter("(\\s|,|\\.|:|;|!|\\?|'|\\\")+"); // se handledning

		while (r.hasNext()) {
			String word = r.next().toLowerCase();

			g.process(word);
		}

		r.close();

		g.report();

		System.out.println(" ");

		long t1 = System.nanoTime();
		System.out.println("tid: " + (t1 - t0)/1000000.0 + " ms");
	}

}
package textproc;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class MultiWordCounter implements TextProcessor {
    private Map<String, Integer> ord = new TreeMap<>();

    public MultiWordCounter(String[] ord) {
        for(String s : ord){
            this.ord.put(s, 0);
        }
    }

    public void process(String w) {
        if (ord.containsKey(w)){
            ord.replace(w, ord.get(w), ord.get(w)+1);
        }
    }

    public void report() {
        for (String key : ord.keySet()) {
            System.out.println(key + ": " + ord.get(key));
        }

    }
}

package textproc;

import java.util.*;

public class GeneralWordCounter implements TextProcessor {
    private Map<String, Integer> ord = new TreeMap<>();
    private Set<String> undantag;

    public GeneralWordCounter(Set<String> undanT) {
        this.undantag = undanT;
    }

    public void process(String w) {
        if(!undantag.contains(w)){
            if(ord.containsKey(w)){
                ord.replace(w, ord.get(w), ord.get(w)+1);
            } else {
                ord.put(w, 1);
            }
        }
    }


    public void report() {
        Set<Map.Entry<String, Integer>> wordSet = ord.entrySet();
        List<Map.Entry<String, Integer>> wordList = new ArrayList<>(wordSet);
        wordList.sort(new WordCountComparator());
        for (int i = 0; i < 5; i++){
            System.out.println(wordList.get(i).getKey() + ": " + wordList.get(i).getValue());
        }
//        for (String key : ord.keySet()) {
//            if (ord.get(key) >= 200) {
//                System.out.println(key + ": " + ord.get(key));
//            }
//        }
    }
}

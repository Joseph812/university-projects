package mountain;

import java.util.HashMap;

import fractal.*;

public class Mountain extends Fractal {
	private Point a, b, c;
	
	public Mountain(int a1, int a2, int b1, int b2, int c1,int c2) {
		super();
		a = new Point(a1, a2);
		b = new Point(b1, b2);
		c = new Point(c1, c2);
	}
	
	private HashMap<Side, Point> hm = new HashMap<>();
			
	public String getTitle() {
		return "Mountain Triangle";
	}
	
	private void drawTriangle(TurtleGraphics turtle, Point p1, Point p2, Point p3) {
		turtle.moveTo(p1.getX(), p1.getY());
		turtle.penDown();
		turtle.forwardTo(p2.getX(), p2.getY());
		turtle.forwardTo(p3.getX(), p3.getY());
		turtle.forwardTo(p1.getX(), p1.getY());
		turtle.penUp();
	}
	
//	private void drawCorner(TurtleGraphics turtle, Point p1, Point p2, Point p3) {
//		turtle.moveTo(p1.getX(), p1.getY());
//		turtle.penDown();
//		turtle.forwardTo(p2.getX(), p2.getY());
//		turtle.forwardTo(p3.getX(), p3.getY());
//		turtle.penUp();
//	}
//	
//	private void drawLine(TurtleGraphics turtle, Point p1, Point p2, Point p3) {
//		turtle.moveTo(p1.getX(), p1.getY());
//		turtle.penDown();
//		turtle.forwardTo(p2.getX(), p2.getY());
//		turtle.penUp();
//	}
	
	public void draw(TurtleGraphics turtle) {
		turtle.moveTo(a.getX(),a.getY());
		fractalLine(turtle, order, a, b, c, 25);
	}
	
	private void fractalLine(TurtleGraphics turtle, int order, Point p1, Point p2, Point p3, double dev) {
		if(order == 0) {
			drawTriangle(turtle, p1, p2, p3);
		} else {
		//	RandomUtilities R = new RandomUtilities();
		//	Double randomP1XFactor = R.randFunc(p1.getX());
			
			Point x, y, z;
			
			if(hm.containsKey(new Side(p1, p2))) {
				x = hm.remove(new Side(p1, p2));
			} else {
				x = new Point(((p1.getX()+p2.getX())/2), ((p1.getY()+p2.getY())/2)  + (int) RandomUtilities.randFunc((dev)));
				hm.put(new Side(p1, p2), x);
			}
				
			if(hm.containsKey(new Side(p2, p3))) {
				y = hm.remove(new Side(p2, p3));
			} else {
				y = new Point((p2.getX()+p3.getX())/2, (((p2.getY()+p3.getY())/2)) + (int) RandomUtilities.randFunc((dev/2)));
				hm.put(new Side(p2, p3), y);
				
			}
			
			if(hm.containsKey(new Side(p1, p3))) {
				z = hm.remove(new Side(p1, p3));
			} else {
				z = new Point((p1.getX()+p3.getX())/2, (((p1.getY()+p3.getY())/2) + (int) RandomUtilities.randFunc((dev/2))));
				hm.put(new Side(p1, p3), z);
			}
				
			fractalLine(turtle, order-1, x, y, z, dev/2);
			fractalLine(turtle, order-1, p2,x, y, dev/2);
			fractalLine(turtle, order-1, y,	p3, z, dev/2);
			fractalLine(turtle, order-1, x,	z, p1, dev/2);

		}
	}
	
//	private void fractalLineC(TurtleGraphics turtle, int order, Point p1, Point p2, Point p3) {
//		if(order == 0) {
//			drawCorner(turtle, p1, p2, p3);
//		} else {
//			fractalLine(turtle, order-1, 	new Point((p1.getX()+p2.getX())/2, (p1.getY()+p2.getY())/2), 
//											new Point((p2.getX()+p3.getX())/2, (p2.getY()+p3.getY())/2), 
//											new Point((p1.getX()+p3.getX())/2, (p1.getY()+p3.getY())/2));
//
//			fractalLineL(turtle, order-1, 	new Point((p2.getX()+p3.getX())/2, (p2.getY()+p3.getY())/2), 
//											p3,
//											new Point((p1.getX()+p3.getX())/2, (p1.getY()+p3.getY())/2));
//
//			fractalLineL(turtle, order-1, 	new Point((p1.getX()+p2.getX())/2, (p1.getY()+p2.getY())/2), 
//											p1, 
//											new Point((p1.getX()+p3.getX())/2, (p1.getY()+p3.getY())/2));
//
//			fractalLineC(turtle, order-1, 	new Point((p1.getX()+p2.getX())/2, (p1.getY()+p2.getY())/2), 
//											p2,
//											new Point((p2.getX()+p3.getX())/2, (p2.getY()+p3.getY())/2));
//		}
//	}
//	
//	private void fractalLineL(TurtleGraphics turtle, int order, Point p1, Point p2, Point p3) {
//		if(order == 0) {
//			drawLine(turtle, p1, p2, p3);
//		} else {
//			fractalLineC(turtle, order-1, 	new Point((p1.getX()+p2.getX())/2, (p1.getY()+p2.getY())/2), 
//											new Point((p2.getX()+p3.getX())/2, (p2.getY()+p3.getY())/2), 
//											new Point((p1.getX()+p3.getX())/2, (p1.getY()+p3.getY())/2));
//
//			fractalLineC(turtle, order-1, 	new Point((p2.getX()+p3.getX())/2, (p2.getY()+p3.getY())/2), 
//											p3,
//											new Point((p1.getX()+p3.getX())/2, (p1.getY()+p3.getY())/2));
//
//			fractalLineC(turtle, order-1, 	new Point((p1.getX()+p2.getX())/2, (p1.getY()+p2.getY())/2), 
//											p1, 
//											new Point((p1.getX()+p3.getX())/2, (p1.getY()+p3.getY())/2));
//
//			fractalLine(turtle, order-1, 	new Point((p1.getX()+p2.getX())/2, (p1.getY()+p2.getY())/2), 
//											p2,
//											new Point((p2.getX()+p3.getX())/2, (p2.getY()+p3.getY())/2));
//		}
//	}
}

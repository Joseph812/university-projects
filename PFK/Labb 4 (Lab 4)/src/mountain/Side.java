package mountain;

public class Side {
	Point a, b;
	
	public Side(Point a, Point b) {
		super();
		this.a = a;
		this.b = b;
	}
	

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Side) {
			Side p = (Side) obj;
			return a == p.a && b == p.b || a == p.b && b == p.a;
		} else {
			return false;
		}
	}
	
	@Override
	public int hashCode() {
	    return  a.hashCode() + b.hashCode();
	}
}

package lab3;

import java.util.*;

public class WordCountComparator implements Comparator<Map.Entry<String, Integer>> {

    public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
        if(o1.getValue().equals(o2.getValue())) {
            return o1.getKey().compareTo(o2.getKey());
        }
        else return o2.getValue() - o1.getValue();
    }
}

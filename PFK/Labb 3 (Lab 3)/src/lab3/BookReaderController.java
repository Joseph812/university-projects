package lab3;

import java.io.File;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Scanner;
import java.util.Set;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;

public class BookReaderController extends Application {

	public void start(Stage primaryStage) throws Exception {
		BorderPane root = new BorderPane();
		
		Scene scene = new Scene(root, 500, 500);
		primaryStage.setTitle("BookReader");
		primaryStage.setScene(scene);
		primaryStage.show();
		
		Scanner scan = new Scanner(new File("undantagsord.txt"));
        Set<String> stopwords = new HashSet<>();
        while(scan.hasNext()) {
            stopwords.add(scan.next().toLowerCase());
        }
        scan.close();
        GeneralWordCounter counter = new GeneralWordCounter(stopwords);
        Scanner z = new Scanner(new File("nilsholg.txt"));
        z.findWithinHorizon("\uFEFF", 1);
        z.useDelimiter("(\\s|,|\\.|:|;|!|\\?|'|\\\")+");
        while (z.hasNext()) {
            String word = z.next().toLowerCase();
            counter.process(word);
        }
    
        ObservableList<Map.Entry<String, Integer>> words = FXCollections.observableArrayList(counter.getWords());
        ListView<Map.Entry<String, Integer>> listView = new ListView<Map.Entry<String, Integer>>(words);
        
        root.setCenter(listView);
        
        HBox hbox = new HBox();
        ToggleGroup toggle = new ToggleGroup();
        RadioButton alphab = new RadioButton("Alphabetic");
        RadioButton freqb = new RadioButton("Frequecy");
        alphab.setToggleGroup(toggle);
        freqb.setToggleGroup(toggle);
        TextField txt = new TextField();
        Button sök = new Button("Search");
        Alert alert = new Alert(AlertType.ERROR);
        alert.setTitle("IllegalArgumentException");
        alert.setHeaderText("I'm sorry but there does not seem to be a word like that in the entire text.");
        sök.setDefaultButton(true);
        alphab.setOnAction(e -> words.sort((e1, e2) -> e1.getKey().compareTo(e2.getKey())));
        freqb.setOnAction(e -> words.sort((e1, e2) -> e2.getValue() - e1.getValue()));
        sök.setOnAction(e -> {
        	String c = txt.getText();
        	for(Map.Entry<String, Integer> i:words) {
        		if(i.getKey().equals(c)) {
        			listView.scrollTo(i);
        			return;
        		} 
        		
        	}
        	alert.showAndWait().filter(response -> response == ButtonType.OK).ifPresent(response -> alert.close());
        	 
        //	listView.setOnScrollTo(f -> words.);
        	
        	
        });
        hbox.getChildren().add(alphab);
        hbox.getChildren().add(freqb);
        hbox.getChildren().add(txt);
        hbox.getChildren().add(sök);
        
        root.setBottom(hbox);
	}

	public static void main(String[] args) {
		Application.launch(args);

	}

}

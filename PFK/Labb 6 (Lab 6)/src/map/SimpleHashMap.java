package map;

import java.util.Random;

public class SimpleHashMap<K, V> implements Map<K, V> {
	private Entry<K, V>[] table;
	private int size;
	private Double loadfactor;
	private int capacity;
	
	
	public SimpleHashMap() {
		loadfactor = 0.75;
		capacity = 16;
		table = (Entry<K, V>[]) new Entry[capacity];
		size = 0;
	}
	
	public SimpleHashMap(int capacity) {
		loadfactor = 0.75;
		this.capacity = capacity;
		table = (Entry<K, V>[]) new Entry[capacity];
		size = 0;
	}
	
	private int index(K key) {
		return Math.abs(key.hashCode() % table.length);
	}
	
	private Entry<K, V> find(int index, K key){
		Entry<K, V> entry = table[index];
		while(entry != null) {
			if(entry.key.equals(key)) {
				return entry;
			} else {
				entry = entry.next;
			}
		}
		return null;
	}
	
	public boolean isEmpty(){
		return size() == 0;
	}
	
	public int size() {
		return size;
	}
	
	private void rehash() {
		SimpleHashMap<K, V> newTable = new SimpleHashMap<>(table.length*2);
		for(int i = 0; i < table.length; i++) {
			Entry<K,V> entry = table[i];
			while( entry != null) {
				newTable.put(entry.getKey(), entry.getValue());
				entry = entry.next;
			}
		}
		table = newTable.table;
	}
	
	@Override
	public V get(Object O) {
		K key;
		try {
			key = (K) O;
		} catch (Exception e) {
			return null;
		}
		int i = index(key);
		Entry<K, V> ent = find(i, key);
		if(ent == null) {
			return null;
		}
		return ent.getValue();
	}

	@Override
	public V put(K arg0, V arg1) {
		if(size > table.length * loadfactor) {
			rehash();
		}
		int i = index(arg0);
		if(table[i] == null) {
			table[i] = new Entry<>(arg0, arg1);
			size++;
			return null;
		}
		Entry<K, V> entry = table[i];
		while(entry != null) {
			if(entry.getKey().equals(arg0)) {
				return entry.setValue(arg1);
			}
			if(entry.next == null) {
				entry.next = new Entry<K, V>(arg0, arg1);
				size++;
				return null;
			}
			entry = entry.next;
		}
		return null;
	}

	@Override
	public V remove(Object  O) {
		K key;
		try {
			key =  (K) O;
		} catch (Exception e) {
			return null;
		}
		int i = index(key);
		if(table[i] == null) {
			return null;
		}
		if(table[i].getKey().equals(key)) {
			V v = table[i].getValue();
			table[i] = table[i].next;
			size--;
			return v;
		}
		Entry<K, V> entry = table[i];
		
		while(entry.next != null) {
			if(entry.next.getKey().equals(key)) {
				V v = entry.next.getValue();
				entry.next = entry.next.next;
				size--;
				return v;
			}
			entry = entry.next;
		}
		return null;
	}
	
	String show() {
		String s = "";
		for(int e = 0; e < table.length; e++) {
			s = s + e + " ";
			Entry<K, V> entry = table[e];
			while(entry != null) {
				s = s + "  " + entry.toString();
				entry = entry.next;
			}
			s = s + "\n";
		}
		return s;
	}
	
	private static class Entry<K, V> implements Map.Entry<K, V>{
		private K key;
		private V value;
		private Entry<K, V> next;
		
		public Entry(K k, V v){
			key = k;
			value = v;
			next = null;
		}
		
		public String toString() {
			return key.toString() + " = " + value.toString();
		}

		@Override
		public K getKey() {
			return key;
		}

		@Override
		public V getValue() {
			return value;
		}

		@Override
		public V setValue(V value) {
			V v = this.value;
			this.value = value;
			return v;
		}
		
	}
	
	public static void main(String[] args) {
		SimpleHashMap<Integer, Integer> shm = new SimpleHashMap<>();
		Random random = new Random();
		for(int i = 0; i < 30; i++) {
			int k = random.nextInt(40);
			int v = random.nextInt(40) + 111;
			if(Math.random() > 0.5) {
				k = k*-1;
			}
			if(Math.random() > 0.5) {
				v = v*-1;
			}
			System.out.println(i + "th entry is key " + k + " and value " + v);
			shm.put(k, v);
		}
		
		System.out.println("result is: \n" + shm.show());
		
	}
}

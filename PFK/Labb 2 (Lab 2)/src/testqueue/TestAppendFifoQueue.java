package testqueue;

import static org.junit.Assert.*;

import java.util.Queue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import queue_singlelinkedlist.FifoQueue;

public class TestAppendFifoQueue {
	private FifoQueue<Integer> myQIntQueue;
	private FifoQueue<Integer> myQ2IntQueue;
	
	@Before
	public void setUp() throws Exception {
		myQIntQueue = new FifoQueue<Integer>();
		myQ2IntQueue = new FifoQueue<Integer>();
	}

	@After
	public void tearDown() throws Exception {
		myQIntQueue = null;
		myQ2IntQueue = null;
	}
	@Test
	public void testBothEmpty() {
		myQIntQueue.append(myQ2IntQueue);
		assertTrue("fel size", myQIntQueue.size() == 0);
		assertTrue("fel size", myQ2IntQueue.size() == 0);
	}

	@Test
	public void testEmptyAppendsQ() {
		myQ2IntQueue.offer(1);
		myQ2IntQueue.offer(2);
		myQ2IntQueue.offer(3);
		myQIntQueue.append(myQ2IntQueue);
		assertTrue("fel size", myQIntQueue.size() == 3);
	}

	@Test
	public void testQAppendsEmpty() {
		myQIntQueue.offer(1);
		myQIntQueue.offer(2);
		myQIntQueue.offer(3);
		myQIntQueue.append(myQ2IntQueue);
		assertTrue("fel size", myQIntQueue.size() == 3);
	}

	@Test
	public void testQAppendQ2() {
		myQ2IntQueue.offer(4);
		myQ2IntQueue.offer(5);
		myQ2IntQueue.offer(6);
		myQIntQueue.offer(1);
		myQIntQueue.offer(2);
		myQIntQueue.offer(3);
		myQIntQueue.append(myQ2IntQueue);
		System.out.println(myQIntQueue);
		assertTrue("fel size", myQIntQueue.size() == 6);
	}

	@Test
	public void testQappendQ() {
		myQIntQueue.offer(1);
		myQIntQueue.offer(2);
		myQIntQueue.offer(3);
		try {
			myQIntQueue.append(myQIntQueue);
		} catch (IllegalArgumentException e) {
			assertTrue("fel", true);
		}
		assertTrue("fel size", myQIntQueue.size() == 3);
	}

}

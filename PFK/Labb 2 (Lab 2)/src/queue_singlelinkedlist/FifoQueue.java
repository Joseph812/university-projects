package queue_singlelinkedlist;
import java.util.*;

public class FifoQueue<E> extends AbstractQueue<E> implements Queue<E> {
	private QueueNode<E> last;
	private int size;

	public FifoQueue() {
		super();
		last = null;
		size = 0;
	}

	/**	
	 * Inserts the specified element into this queue, if possible
	 * post:	The specified element is added to the rear of this queue
	 * @param	e the element to insert
	 * @return	true if it was possible to add the element 
	 * 			to this queue, else false
	 */
	public boolean offer(E e) {
		if(last == null){
			last = new QueueNode<>(e);
			last.next = last;
			size++;
		} else {
			QueueNode<E> x = new QueueNode<>(e);
			x.next = last.next;
			last.next = x;
			last = x;
			size++;
		}
			
		return true;
	}
	
	/**	
	 * Returns the number of elements in this queue
	 * @return the number of elements in this queue
	 */
	public int size() {
		return size;
	}
	
	/**	
	 * Retrieves, but does not remove, the head of this queue, 
	 * returning null if this queue is empty
	 * @return 	the head element of this queue, or null 
	 * 			if this queue is empty
	 */
	public E peek() {
		if (last == null) return null;
		else return last.next.element;
	}

	/**	
	 * Retrieves and removes the head of this queue, 
	 * or null if this queue is empty.
	 * post:	the head of the queue is removed if it was not empty
	 * @return 	the head of this queue, or null if the queue is empty 
	 */
	public E poll() {
		E x = peek();
		if(last != null) {
			if (last.next == last) {
				last = null;
				size--;
			} 
			else { last.next = last.next.next;
			size --;
			}
		}
		return x;
	}
	
	/**	
	 * Returns an iterator over the elements in this queue
	 * @return an iterator over the elements in this queue
	 */	
	public Iterator<E> iterator() {
		return new QueueIterator();
	}
	private class QueueIterator implements Iterator<E>{
		private QueueNode<E> pos;

		private QueueIterator() {
			if(last != null)pos = last.next;
			else pos = null;
		}

		public boolean hasNext(){
			if (pos == null) return false;
			else return true;
		}

		public E next(){
			if (pos == null) throw new NoSuchElementException();
		
			else if(pos == last) {
				QueueNode<E> x = pos;
				pos = null;
				return x.element;
			}
			else {
				QueueNode<E> x = pos;
				pos = pos.next;
				return x.element;
			}
		}
	}
	private static class QueueNode<E> {
		E element;
		QueueNode<E> next;

		private QueueNode(E x) {
			element = x;
			next = null;
		}
	}

	public void append(FifoQueue<E> q) {
		
		if (q != this) {
			if(last == null) {
				last = q.last;
				size = q.size;
			}
			else if(q.last != null) {
				QueueNode<E> x = last.next;
				last.next = q.last.next;
				q.last.next = x;
				last = q.last;
				size = size + q.size;
				q.last = null;
				q.size = 0;
			}
		} else throw new IllegalArgumentException();
	}
}

package sudoku;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.After;
import org.junit.Before;
import org.junit.jupiter.api.Test;

class SudokuTest {
	private Sudoku s = new Sudoku();

	@Before
	public void setUp() throws Exception {
		s = new Sudoku();
	}

	@After
	public void tearDown() throws Exception {
	//	s.clear();
		s = null;
	}
	
	@Test
	void testEmpty() {
		assertTrue(s.Solve());
	}
	
	@Test
	void testUnsolvableRow() {
		s.setValueAt(0, 0, "5");
		s.setValueAt(0, 5, "5");
		assertTrue(!s.Solve());
	}
	
	@Test
	void testUnsolvableCol() {
		s.setValueAt(0, 0, "5");
		s.setValueAt(5, 0, "5");
		assertTrue(!s.Solve());
	}
	
	@Test
	void testUnsolvableField() {
		s.setValueAt(0, 0, "5");
		s.setValueAt(2, 2, "5");
		assertTrue(!s.Solve());
	}
	
	@Test
	void testUnsolvableEmptyPlaces() {
		s.setValueAt(0, 0, "1");
		s.setValueAt(0, 1, "2");
		s.setValueAt(0, 2, "3");
		s.setValueAt(1, 0, "4");
		s.setValueAt(1, 1, "5");
		s.setValueAt(1, 2, "6");
		s.setValueAt(2, 3, "7");
		assertTrue(!s.Solve());
	}
	
	@Test
	void testClear() {
		s.setValueAt(0, 0, "5");
		s.setValueAt(0, 5, "5");
		s.clear();
		assertTrue(!s.Solve());
	}
	
	@Test
	void testSolver() {
		s.setValueAt(5, 0, "1");
		s.setValueAt(3, 7, "2");
		s.setValueAt(0, 2, "3");
		s.setValueAt(6, 7, "4");
		s.setValueAt(7, 6, "5");
		s.setValueAt(2, 2, "6");
		s.setValueAt(8, 7, "7");
		assertTrue(s.Solve());
	}
	
	@Test
	void testGenerate() {
		s.setMatrix(Sudoku.generate(40));
		assertTrue(s.Solve());
	}
}

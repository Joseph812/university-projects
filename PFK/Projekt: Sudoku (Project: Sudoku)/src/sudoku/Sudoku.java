package sudoku;

import java.util.ArrayList;
import java.util.Random;

public class Sudoku {
	private ArrayList<ArrayList<String>> matrix;					// kan vara static
	private Long counter;
	
	/**
	 * Creates an empty Sudoku on initiation.
	 */
	
	public Sudoku() {
		matrix = clear();
		counter = System.currentTimeMillis();
	}
	
	/** 
	 * Returns a matrix with empty strings.
	*/
	public ArrayList<ArrayList<String>> clear() {
		ArrayList<String> r = new ArrayList<>();
		ArrayList<ArrayList<String>> m = new ArrayList<>();
		for(int i = 0; i < 9; i++) {
			r.add("");
		}
		for(int i = 0; i < 9; i++) {
			m.add(r);
		}
	return m;
	}
	
	/**
	 * Gives the value at the specific position of the Sudoku.
	 * @param r Index of row.
	 * @param c Index of column.
	 * @return	String value at specific position.
	 */
	
	public String getValueAt(int r, int c) {
		return matrix.get(r).get(c);
	}
	
	/**
	 * Gives the current matrix.
	 * @return The current matrix.
	 */
	
	public ArrayList<ArrayList<String>> getMatrix(){
		return matrix;
	}
	
	/**
	 * Updates the whole matrix.
	 * @param m New matrix.
	 */
	
	public void setMatrix(ArrayList<ArrayList<String>> m) {
		matrix = m;
	}
	
	/**
	 * Gives the whole row at index r.
	 * @param r Index of row.
	 * @return	The row at r.
	 */
	
	public ArrayList<String> getRow(int r) {
		ArrayList<String> ra = new ArrayList<>();
		ra = matrix.get(r);
		return ra;
	}
	
	/**
	 * Gives the whole column at index c.
	 * @param c Index of column.
	 * @return The column at c.
	 */
	
 	public ArrayList<String> getCol(int c) {
		ArrayList<String> ca = new ArrayList<>();
		for(ArrayList<String> e: matrix) {
			ca.add(e.get(c));
		}
		return ca;
	}
	
 	/**
 	 * Changes the value at a specified position.
 	 * @param r Index of row.
 	 * @param c Index of column.
 	 * @param n New value.
 	 */
 	
	public void setValueAt(int r, int c, String n) {	// kan vara static
		if(Integer.valueOf(n) < 10 && Integer.valueOf(n) > 0) {
			ArrayList<ArrayList<String>> m = new ArrayList<>();
			int i = 0;
			int j = 0;
			for(ArrayList<String> v: matrix) {
				if(i == (r)) {
					ArrayList<String> v2 = new ArrayList<String>();
					for(String x: v) {
						if(j == c) {
							v2.add(n);
							j++;
						} else {
							v2.add(x);
							j++;
						}
					}
				m.add(v2);
				i++;
			} else {
				m.add(v);
				i++;
			}
		}	
		matrix = m;
		}
	}
	
//	private ArrayList<String> getRowCol(int r, int c) {
//		ArrayList<String> RC = new ArrayList<String>();
//		for(String el: matrix.get(r)) {
//			RC.add(el);
//		}
//		for(ArrayList<String> e: matrix) {
//			RC.add(e.get(c));
//		}
//		return RC;
//	}
	
	private void setRow(int r, ArrayList<String> AL) {
		matrix.set(r, AL);
	}
	
	/**
	 * Removes the value at a specified position.
	 * @param r Index of row.
	 * @param c Index of column.
	 */
	
	public void remove(int r, int c) {
		ArrayList<String> row = matrix.get(r);
		row.set(c, "");
		matrix.set(r, row);
	}
	
	private void removeRow(int r) {
		ArrayList<String> AL = new ArrayList<>();
		for(int i = 0; i < 9; i++) {
			AL.add("");
		}
		matrix.set(r, AL);
	}
	
	/**
	 * Checks if a position is occupied.
	 * @param r Index of row.
	 * @param c Index of column.
	 * @return True if occupied else returns false.
	 */
	
	public Boolean isOccupied(int r, int c) {
		return !getValueAt(r, c).equals("");
	}
	
	private ArrayList<String> field(int r, int c) {
		int x = 0;
		int y = 0;
		ArrayList<String> f = new ArrayList<>();
		if(r >= 0 && r < 3) 	y = 1;
		if(r > 2 && r < 6)		y = 4;
		if(r > 5) 				y = 7;
		if(c >= 0 && c < 3) 	x = 1;
		if(c > 2 && c < 6)		x = 4;
		if(c > 5) 				x = 7;
		
		f.add(getValueAt(y, x));
		f.add(getValueAt(y-1, x-1));
		f.add(getValueAt(y-1, x));
		f.add(getValueAt(y, x-1));
		f.add(getValueAt(y+1, x));
		f.add(getValueAt(y, x+1));
		f.add(getValueAt(y+1, x+1));
		f.add(getValueAt(y+1, x-1));
		f.add(getValueAt(y-1, x+1));
		
		return f;
	}
	
	private Boolean tryAdd(int r, int c, int s) {
		String ss = String.valueOf(s);
		if(getCol(c).contains(ss)) return false;
		if(field(r, c).contains(ss)) return false;
		if(getRow(r).contains(ss)) return false;
		return true;
	}
	
	private int countInRCF(int r, int c) {
		int n = 0;
		String v = getValueAt(r, c);
		ArrayList<String> mr = getRow(r);
		ArrayList<String> mc = getCol(c);
		ArrayList<String> mf = field(r, c);
		for(String col: mr) {
			if(col.equals(v)) {
				n++;
			}
		}
		for(String row: mc) {
			if(row.equals(v)) {
				n++;
			}
		}
		n--;
		for(String rowcol: mf) {
			if(rowcol.equals(v)) {
				n++;
			}
		}
		n--;
		return n;
	}
	
	private boolean checkIfCanSolve() {
		for(ArrayList<String> r: matrix) {
			for(String c: r) {
				if(!c.contentEquals("")) {
					if(countInRCF(matrix.indexOf(r), r.indexOf(c)) > 1) {
						return false;
					}
				}
			}
		}
		return true;
	}
	
	private Boolean Solver(int row, int col) {
		if(System.currentTimeMillis() - counter > 5000) {
			return false;
		}
		if (row >= 9) {
			return true;
		}
		if(col == 9) {
			if(Solver(row + 1, 0)) {
				return true;
			} return false;
		}
		if(isOccupied(row, col)) {
			if(col >= 8) {
				 if(Solver(row + 1, 0)) {
					 return true;
				}
			} if(Solver(row, col + 1)) {
				return true;
			}
			return false;
		}
		for(int i = 1; i < 10; i++) {
			if(tryAdd(row, col, i)) {
				setValueAt(row, col, String.valueOf(i));
				if(col == 8) {
					if(Solver(row + 1, 0)) {
						return true;
					}
				}else if(Solver(row, col + 1)) {
					return true;
				}
				remove(row, col);
			}
		}
		return false;
	}
	
	/**
	 * Solves the current matrix.
	 * @return True if solvable, otherwise false.
	 */
	
	public Boolean Solve() {
		if(checkIfCanSolve()) {
			return Solver(0, 0);
		}
		return false;
	}
	
	/**
	 * Solves a given Sudoku.
	 * @param m A Sudoku matrix.
	 * @return	A solved matrix from the matrix given.
	 */
	
	public static ArrayList<ArrayList<String>> Solver(ArrayList<ArrayList<String>> m){
		Sudoku s = new Sudoku();
		s.setMatrix(m);
		s.Solve();
		return s.getMatrix();
	}
	
	private Integer randomInt() {
		Random random = new Random();
		return random.nextInt(9);
	}
	
	private ArrayList<String> randomArray() {
		ArrayList<String> randomAL = new ArrayList<>();
		Random random = new Random();
		int i = random.nextInt(9) + 1;
		while(randomAL.size() != 9) {
			while(randomAL.contains(String.valueOf(i))) {
				i = random.nextInt(9) + 1;
			}
			randomAL.add(String.valueOf(i));
		}
		return randomAL;
	}
	
	private ArrayList<Integer> ALStringToInt(ArrayList<String> AL){
		ArrayList<Integer> IntAL = new ArrayList<>();
		for(String s: AL) {
			IntAL.add(Integer.valueOf(s));
		}
		return IntAL;
	}
	
	private Boolean tryAddRow(int r, ArrayList<Integer> AL) {
		ArrayList<Boolean> checkList = new ArrayList<>();
		for(int s: AL) {
			checkList.add(tryAdd(r, AL.indexOf(s), s));
		}
		return !checkList.contains(false);
	}
	
	private Boolean generateRec(int row, int col ){
		if(row == 9) {
			return true;								// if it is the 9th row then the code is complete and sudoku is generated
		}
		 if(row >= 5) {
			for(int i = 1; i < 10; i++) {
				if(tryAdd(row, col, i)) {
					setValueAt(row, col, String.valueOf(i));
					if(col == 8) {
						if(Solver(row + 1, 0)) {
							return true;
						}
					}else if(Solver(row, col + 1)) {
						return true;
					}
					remove(row, col);
				}
			}
		 }
		
		ArrayList<String> AL = randomArray();			// creates an arraylist with random none-repeated numbers 1-9
		if(tryAddRow(row, ALStringToInt(AL))) {			// checks if it is possible to add the row----reference: *
			setRow(row, AL);							// updates the row
			while(!generateRec(row + 1, col)) {			// while next rows are not working
				removeRow(row);							// remove the row
				AL = randomArray();						// changes the order of numbers in arraylist
				while(!tryAddRow(row, ALStringToInt(AL))) {			// while the corrent row isnt addable
					AL = randomArray();		// change the order again
				}
				setRow(row, AL);						//update the row when it is finally available
			}
			return true;								//return true
		}
		return false;									// if * did not work then return false
	}
	
	/**
	 * Creates a randomly generated Sudoku that is solvable.
	 * @param n Number of removed values at random positions.
	 * @return	A new Sudoku matrix.
	 */
	
	public static ArrayList<ArrayList<String>> generate(int n) {
		Sudoku s = new Sudoku();
		s.generateRec(0, 0);
		for(int i = 0; i < n; i++) {
			int x = s.randomInt();
			int y = s.randomInt();
			while(!s.isOccupied(x, y)) {
				x = s.randomInt();
				y = s.randomInt();
			}
			s.remove(x, y);
		}
		return s.matrix;
	}
	
//	public Boolean equals(ArrayList<ArrayList<String>> m2) {
//		ArrayList<Boolean> xs = new ArrayList<>();
//		for(int x = 0; x < 9; x++) {
//			for(int y = 0; y < 9; y++) {
//				xs.add(getValueAt(x, y).equals(m2.get(x).get(y)));
//			}
//		}
//		return !xs.contains(false);
//	}
	
	private ArrayList<ArrayList<String>> emptyToZeros(){
		ArrayList<ArrayList<String>> m = new ArrayList<>();
		for(ArrayList<String> r : matrix) {
			ArrayList<String> row = r;
			for(String c : r){
				if(c == "") {
					row.set(r.indexOf(c), "0");
				}
			}
			m.add(row);
		}
		return m;
	}
	
	/**
	 * Returns a string that shows all values of Sudoku in right order.
	 */
	
	public String toString() {
		ArrayList<String> s = new ArrayList<>();
		ArrayList<ArrayList<String>> m = emptyToZeros();
		for(ArrayList<String> r : m) {
			r.set(2, r.get(2) + "|");
			r.set(5, r.get(5) + "|");
			s.add(r.toString().substring(1,28).replace(",", "").replace(" ", "") + "\n");
			r.set(2, r.get(2).replace("|", ""));
			r.set(5, r.get(5).replace("|", ""));
		}
		for(int i = 0; i < 11; i++) {s.add(3, "-");}
		s.add(14,"\n");
		for(int i = 0; i < 11; i++) {s.add(18, "-");}
		s.add(29,"\n");
		return s.toString().substring(1, s.toString().length()-1).replace(",", "").replace(" ", "");
	}
}

package Graphics;

import java.util.ArrayList;
import sudoku.*;

import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.stage.Stage;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.TilePane;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TextFormatter.Change;

public class SudokuApplication extends Application{
    String title = "Sudoku Solver";
    
    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        BorderPane root = new BorderPane();
        
        Scene scene = new Scene(root, 520, 570);
        stage.setTitle(title);
        stage.setScene(scene);
        stage.show();

        TilePane sudoku = gridMaker();
        root.setCenter(sudoku);
        
        Alert alert = new Alert(AlertType.ERROR);
        alert.setTitle("IllegalArgumentException");
        alert.setHeaderText("It would seem that this sudoku is quite impossible to solve. Please do try again");
            
        Button solve = buttonMaker("Solve");
        solve.setOnAction(e -> {
            ArrayList<ArrayList<String>> before = sendSudoku(sudoku);
            ArrayList<ArrayList<String>> after = Sudoku.Solver(before);
            if(after.equals(before)) {
                alert.showAndWait().filter(response -> response == ButtonType.OK).ifPresent(response -> alert.close());
            } else {
                buildSudoku(after, sudoku);
            }
        });
        solve.setDefaultButton(true);
        
        Button clear = buttonMaker("Clear");
        clear.setOnAction(e -> clear(sudoku));
        clear.setCancelButton(true);
        
        Button generateEasy = buttonMaker("Easy");
        generateEasy.setOnAction(e -> buildSudoku(Sudoku.generate(10), sudoku));
        
        Button generate = buttonMaker("Medium");
        generate.setOnAction(e -> buildSudoku(Sudoku.generate(40), sudoku));
        
        Button generateHard = buttonMaker("Hard");
        generateHard.setOnAction(e -> buildSudoku(Sudoku.generate(60), sudoku));

        HBox hbox = new HBox();
        hbox.getChildren().addAll(solve, clear, generateEasy, generate, generateHard);
        hbox.setMaxHeight(50);
        hbox.setMinHeight(50);
        root.setBottom(hbox);
    }
    
    private TilePane gridMaker() {
    	TilePane grid = new TilePane();    
        grid.setPrefColumns(3);
        grid.setPrefRows(3);
        grid.setHgap(15);
        grid.setVgap(15);
        for(int x = 0; x < 9; x++) {
            TilePane tiles = new TilePane();
            tiles.setPrefColumns(3);
            tiles.setPrefRows(3);
            tiles.setHgap(5);
            tiles.setVgap(5);
            for(int i = 0; i < 9; i++) {
                TextField tf = new TextField();
                tf.setMaxSize(50, 50);
                tf.setMinSize(50, 50);
                tf.setAlignment(Pos.CENTER);
                tiles.getChildren().add(tf);    
                
                tf.setTextFormatter(new TextFormatter<String>((Change change) ->  {
                    String newText = change.getControlNewText();
                    if (newText.length() > 1) {
                        return null;
                    } else if (newText.matches("[^1-9]")) {
                        return null;
                    } else {
                        return change;
                    }
                }));
            }
            grid.getChildren().add(tiles);
        }
        return grid;
    }
    
    private Button buttonMaker(String name) {
        Button b = new Button(name);
        b.setMinSize(100, 50);
        b.setMaxSize(100, 50);
        return b;
    }
    
    private void clear(TilePane sudoku) {
        ObservableList<Node> s = sudoku.getChildren();
        
        for(int x = 0; x < 9; x++) {
            TilePane tp = (TilePane) s.get(x);
            for(int y = 0; y < 9; y++) {
                ((TextField) tp.getChildren().get(y)).setText("");
            }
        }
    }
    
    private void buildSudoku(ArrayList<ArrayList<String>> matrix, TilePane sudoku) {
        ObservableList<Node> s = sudoku.getChildren();
        
        int counter = 0;
        
        for(int x = 0; x < 3; x++) {
            for(int y = 0; y < 3; y++) {
                TilePane tp = (TilePane) s.get(counter);
                ((TextField) tp.getChildren().get(0)).setText(matrix.get(0).remove(0));
                ((TextField) tp.getChildren().get(1)).setText(matrix.get(0).remove(0));
                ((TextField) tp.getChildren().get(2)).setText(matrix.get(0).remove(0));
                ((TextField) tp.getChildren().get(3)).setText(matrix.get(1).remove(0));
                ((TextField) tp.getChildren().get(4)).setText(matrix.get(1).remove(0));
                ((TextField) tp.getChildren().get(5)).setText(matrix.get(1).remove(0));
                ((TextField) tp.getChildren().get(6)).setText(matrix.get(2).remove(0));
                ((TextField) tp.getChildren().get(7)).setText(matrix.get(2).remove(0));
                ((TextField) tp.getChildren().get(8)).setText(matrix.get(2).remove(0));
                counter++;
            }
            matrix.remove(0);
            matrix.remove(0);
            matrix.remove(0);
        }
        
    }
    
    private <E> ArrayList<ArrayList<String>> sendSudoku(TilePane sudoku){
        ObservableList<Node> s = sudoku.getChildren();
        ArrayList<ArrayList<String>> matrix = new ArrayList<>();
        for(int z = 0; z < 3; z++) {
            ArrayList<String> xs = new ArrayList<String>();
            ArrayList<String> ys = new ArrayList<String>();
            ArrayList<String> zs = new ArrayList<String>();
            for(int x = 0; x < 3; x++) {
                TilePane tp = (TilePane) s.get(x + z*3);
                for(int y = 0; y < 3; y++) {
                    xs.add(((TextField) tp.getChildren().get(y)).getText());
                    ys.add(((TextField) tp.getChildren().get(y+3)).getText());
                    zs.add(((TextField) tp.getChildren().get(y+6)).getText());
                }
            }
            matrix.add(xs);
            matrix.add(ys);
            matrix.add(zs);
        }
            
        return matrix;
    }
}
